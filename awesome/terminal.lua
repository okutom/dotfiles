local terminal = {}

local function get_terminal_path(filename)
    local dir = {"/usr/bin/", "/usr/local/bin/"}
    for _, v in pairs(dir) do
        local f = io.open(v..filename, "r")
        if f ~= nil then
            io.close(f)
            return tostring(v..filename)
        end
    end
    return nil
end

local terminals = {
    "alacritty",
    "hyper",
    "kitty",
    "terminator",
    "konsole",
    "gnome-terminal",
    "xfce4-terminal",
    "mate-terminal",
    "lxterminal",
    "sakura",
    "yakuake",
    "guake",
    "tilda",
    "urxvt",
    "eterm",
    "st",
    "xterm",
    "roxterm",
    "x-terminal-emulator"
}

for _, temp in ipairs(terminals) do
    local result = get_terminal_path(temp)
    if result ~= nil then
        terminal =  result
        break
    end
end

return terminal
