#!/usr/bin/env bash

ESC=$(printf '\033')

if ! type konsole &>/dev/null; then
  printf "${ESC}[31mSKIP: konsole is not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

mkdir -p "${HOME}/.local/share/konsole"
ln -snfv "${abs_script_dir}/Default.profile" "${HOME}/.local/share/konsole/Default.profile"
ln -snfv "${abs_script_dir}/Monokai.colorscheme" "${HOME}/.local/share/konsole/Monokai.colorscheme"

unset abs_script_dir
unset script_dir
