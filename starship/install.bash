#!/usr/bin/env bash

ESC=$(printf '\033')

if ! type starship &>/dev/null; then
  printf "${ESC}[31mSKIP: starship is not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

ln -snfv "${abs_script_dir}" "${HOME}/.config/starship"

unset abs_script_dir
unset script_dir
