#!/usr/bin/env zsh

# Load command-not-found on Debian based distributions
if [[ -s /etc/zsh_command_not_found ]]; then
  source /etc/zsh_command_not_found
fi

alias update='sudo apt update && sudo apt upgrade'
alias upgrade='sudo apt update && sudo apt full-upgrade'
alias autoremove='sudo apt autoremove && sudo apt autoclean'
alias listfile='dpkg-query -L'
