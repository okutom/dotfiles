return {
    'jake-stewart/multicursor.nvim',
    lazy = true,
    branch = '1.0',
    event = 'VeryLazy',
    opts = {},
    config = function()
        local mc = require('multicursor-nvim')
        mc.setup()

        -- add cursor
        vim.keymap.set(
            { 'n', 'v' },
            '<leader>mn',
            function() mc.lineAddCursor(-1) end,
            { desc = 'Add cursor & go to above line' }
        )
        vim.keymap.set(
            { 'n', 'v' },
            '<leader>mt',
            function() mc.lineAddCursor(1) end,
            { desc = 'Add cursor & go to below line' }
        )

        -- skip cursor
        vim.keymap.set(
            { 'n', 'v' },
            '<leader>mN',
            function() mc.lineSkipCursor(-1) end,
            { desc = 'Skip cursor & go to above line' }
        )
        vim.keymap.set(
            { 'n', 'v' },
            '<leader>mT',
            function() mc.lineSkipCursor(1) end,
            { desc = 'Skip cursor & go to below line' }
        )

        -- add cursor by matching
        vim.keymap.set(
            { 'n' },
            '<leader>mmn',
            function() mc.matchAddCursor(1) end,
            { desc = 'Add cursor & go to next matching word' }
        )
        vim.keymap.set(
            { 'v' },
            '<leader>mmn',
            function() mc.matchAddCursor(1) end,
            { desc = 'Add cursor & go to next matching selection' }
        )
        vim.keymap.set(
            { 'n' },
            '<leader>mmN',
            function() mc.matchAddCursor(-1) end,
            { desc = 'Add cursor & go to prev matching word' }
        )
        vim.keymap.set(
            { 'v' },
            '<leader>mmN',
            function() mc.matchAddCursor(-1) end,
            { desc = 'Add cursor & go to prev matching selection' }
        )

        -- skip cursor by matching
        vim.keymap.set(
            { 'n' },
            '<leader>mms',
            function() mc.matchSkipCursor(1) end,
            { desc = 'Skip cursor & go to next matching word' }
        )
        vim.keymap.set(
            { 'v' },
            '<leader>mms',
            function() mc.matchSkipCursor(1) end,
            { desc = 'Skip cursor & go to next matching selection' }
        )
        vim.keymap.set(
            { 'n' },
            '<leader>mmS',
            function() mc.matchSkipCursor(-1) end,
            { desc = 'Skip cursor & go to prev matching word' }
        )
        vim.keymap.set(
            { 'v' },
            '<leader>mmS',
            function() mc.matchSkipCursor(-1) end,
            { desc = 'Skip cursor & go to prev matching selection' }
        )

        -- add all matches
        vim.keymap.set(
            { 'n' },
            '<leader>mma',
            mc.matchAllAddCursors,
            { desc = 'Add cursor by all matching word' }
        )
        vim.keymap.set(
            { 'v' },
            '<leader>mma',
            mc.matchAllAddCursors,
            { desc = 'Add cursor by all matching selection' }
        )

        -- delete cursor
        vim.keymap.set(
            { 'n', 'v' },
            '<leader>mx',
            mc.deleteCursor,
            { desc = 'Delete the current cursor' }
        )

        -- rotate the cursor
        vim.keymap.set(
            { 'n', 'v' },
            '<leader>mk',
            mc.prevCursor,
            { desc = 'Rotate prev cursor' }
        )
        vim.keymap.set(
            { 'n', 'v' },
            '<leader>ms',
            mc.nextCursor,
            { desc = 'Rotate next cursor' }
        )

        -- add/remove cursors with alt + left click
        vim.keymap.set(
            { 'n' },
            '<a-leftmouse>',
            mc.handleMouse,
            { desc = 'toggle cursors with alt+left_mb' }
        )

        -- add/remove the current cursor
        vim.keymap.set(
            { 'n', 'v' },
            '<leader>mq',
            mc.toggleCursor,
            { desc = 'Toggle the current cursor' }
        )

        -- easy delete with esc
        vim.keymap.set(
            { 'n' },
            '<esc>',
            function()
                if not mc.cursorsEnabled() then
                    mc.enableCursors()
                elseif mc.hasCursors() then
                    mc.clearCursors()
                else
                    -- default <esc> handler
                end
            end
        )

        -- restore cursors
        vim.keymap.set(
            { 'n' },
            '<leader>mr',
            mc.restoreCursors,
            { desc = 'Restore current cursors' }
        )

        -- split visual selections by regex
        vim.keymap.set(
            { 'v' },
            '<leader>mS',
            mc.splitCursors,
            { desc = 'split visual selections by regex' }
        )

        -- Append/insert for each line of visual selections
        vim.keymap.set(
            { 'v' },
            'I',
            mc.insertVisual,
            { desc = 'insert for each line of visual selections' }
        )
        vim.keymap.set(
            { 'v' },
            'A',
            mc.appendVisual,
            { desc = 'append for each line of visual selections' }
        )

        -- match new cursors within visual selections by regex
        vim.keymap.set(
            { 'v' },
            'M',
            mc.matchCursors,
            { desc = 'match new cursors within visual selections by regex' }
        )

        -- Customize how cursors look.
        local hl = vim.api.nvim_set_hl
        hl(0, 'MultiCursorCursor', { link = 'Cursor' })
        hl(0, 'MultiCursorVisual', { link = 'Visual' })
        hl(0, 'MultiCursorSign', { link = 'SignColumn' })
        hl(0, 'MultiCursorDisabledCursor', { link = 'Visual' })
        hl(0, 'MultiCursorDisabledVisual', { link = 'Visual' })
        hl(0, 'MultiCursorDisabledSign', { link = 'SignColumn' })
    end
}
