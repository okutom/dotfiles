$script_dir = Split-Path $MyInvocation.MyCommand.Path

New-Item -ItemType SymbolicLink -Value "$script_dir/gitconfig" "$HOME/.gitconfig" -Force
Set-ItemProperty -Path "$HOME/.gitconfig" -Name Attributes -value 'Hidden'
