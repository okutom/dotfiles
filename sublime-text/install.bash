#!/usr/bin/env bash

ESC=$(printf '\033')

if [ ! -e /opt/sublime_text ]; then
  printf "${ESC}[31mSKIP: sublime-text is not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

sublime_config_dir="${HOME}/.config/sublime-text/Packages/User"
mkdir -p "${sublime_config_dir}"
ln -sfv "${abs_script_dir}/Default.sublime-keymap" "${sublime_config_dir}/Default (Linux).sublime-keymap"
ln -sfv "${abs_script_dir}/Monokai.sublime-color-scheme" "${sublime_config_dir}/Monokai.sublime-color-scheme"
ln -sfv "${abs_script_dir}/Preferences.sublime-settings" "${sublime_config_dir}/Preferences.sublime-settings"
ln -sfv "${abs_script_dir}/../vim/vimscript/astarte.vim" "${sublime_config_dir}/.neovintageousrc"
ln -sfv "${abs_script_dir}/Package Control.sublime-settings" "${sublime_config_dir}/Package Control.sublime-settings"

unset sublime_config_dir
unset abs_script_dir
unset script_dir
