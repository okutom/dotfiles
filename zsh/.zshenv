#!/usr/bin/env zsh

## ZDOTDIR ##
export ZDOTDIR="${HOME}/.zsh.d"


## PATH ##
# Ensure path arrays do not contain duplicates.
typeset -gU cdpath fpath mailpath path
# library_path
typeset -T LD_LIBRARY_PATH ld_library_path
typeset -T LD_LIBRARY_PATH_64 ld_library_path_64

# Set the list of directories that Zsh searches for programs.
path=(
  "${HOME}"/.cargo/bin(N-/)
  "${HOME}"/.local/share/flatpak/exports/bin(N-/)
  "${HOME}"/.local/{bin,sbin}(N-/)
  /var/lib/flatpak/exports/bin(N-/)
  /usr/local/{bin,sbin}(N-/)
  /usr/{bin,sbin}(N-/)
  /{bin,sbin}(N-/)
  /snap/bin(N-/)
  $path
)
fpath=(
  "${ZDOTDIR}"/function/*(N-/)
  $fpath
)
ld_library_path=(
  "${HOME}"/.local/lib(N-/)
  /usr/local/lib(N-/)
  /usr/lib(N-/)
  /lib(N-/)
)
ld_library_path_64=(
  "${HOME}"/.local/lib64(N-/)
  /usr/local/lib64(N-/)
  /usr/lib64(N-/)
  /lib64(N-/)
)


## Language ##
if [[ "${TERM}" == (dumb|linux|xterm) ]]; then
  export LANG='en_US.UTF-8'
else
  export LANG='ja_JP.UTF-8'
fi
