{ config, pkgs, pkgs-unstable, ... }:
let
  patchelfFixes = pkgs.patchelfUnstable.overrideAttrs (_finalAttrs: _previousAttrs: {
    src = pkgs.fetchFromGitHub {
      owner = "Patryk27";
      repo = "patchelf";
      rev = "527926dd9d7f1468aa12f56afe6dcc976941fedb";
      sha256 = "sha256-3I089F2kgGMidR4hntxz5CKzZh5xoiUwUsUwLFUEXqE=";
    };
  });
  pcloudPatched = pkgs.pcloud.overrideAttrs (_finalAttrs:previousAttrs: {
    nativeBuildInputs = previousAttrs.nativeBuildInputs ++ [ patchelfFixes ];
  });

  stablePackages = with pkgs; [
    # gui apps
    cage
    pcloudPatched

    # cli apps
    (lib.hiPrio clang)
    delta
    gcc
    procs
    rustup
    sd
    unzip
    # for neovim
      luajitPackages.argparse
      luajitPackages.luafilesystem
      luajitPackages.luarocks
  ];

  unstablePackages = with pkgs-unstable; [
    neovim
  ];
in
{
  home.username = "okutom";
  home.homeDirectory = "/home/okutom";
  home.stateVersion = "24.11";

  home.packages = stablePackages ++ unstablePackages;

  home.file = {};

  home.sessionVariables = {};

  programs = {
    alacritty.enable = true;
    bat.enable = true;
    bottom.enable = true;
    eza.enable = true;
    fd.enable = true;
    # git is installed via configuration.nix
    # git.enable = true;
    home-manager.enable = true;
    # neovim is installed via home.packages
    # neovim.enable = true;
    nushell.enable = true;
    ripgrep.enable = true;
    skim.enable = true;
    starship.enable = true;
    zed-editor.enable = true;
    zellij.enable = false;
    # zsh is installed via configuration.nix
    # zsh.enable = true;
  };

  nixpkgs.config.allowUnfree = true;
}
