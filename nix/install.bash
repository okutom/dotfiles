#!/usr/bin/env bash

ESC=$(printf '\033')

if ! type nixos-rebuild &>/dev/null; then
  printf "${ESC}[31mError: this os is not nixos.${ESC}[m\n"
  exit
fi

if [[ "${1}" = "" ]]; then
  printf "${ESC}[31mError: option for nixos-rebuild is required.${ESC}[m"
  exit
fi

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

sudo cp "${abs_script_dir}/configuration.nix" /etc/nixos
sudo cp "${abs_script_dir}/hardware-configuration.nix" /etc/nixos
sudo cp "${abs_script_dir}/flake.nix" /etc/nixos

sudo nixos-rebuild "${1}" --flake /etc/nixos "${@:2}"

unset abs_script_dir
unset script_dir
