return {
    'nvimdev/dashboard-nvim',
    dependencies = {
        'nvim-tree/nvim-web-devicons',
    },
    lazy = true,
    event = 'VimEnter',
    opts = {
        theme = 'hyper',
        disable_move = true,
        shortcut_type = 'number',
        config = {
            header = {
                ' ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗',
                ' ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║',
                ' ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║',
                ' ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║',
                ' ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║',
                ' ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝',
            },
            disable_move = true,
            shortcut = {
                { desc = 'v' .. vim.version().major .. '.' .. vim.version().minor .. '.' .. vim.version().patch, },
            },
            footer = {},
        },
    },
}
