local awful = require("awful")

local autostart = {}

-- run_onceの仕様上1つ目のスペースまでにコマンドを記述する必要がある
autostart.commands = {
    "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1",
    "blueman-applet",
    -- "caffeine",
    "fcitx5",
    "/usr/lib/geoclue-2.0/demos/agent",
    --"/usr/lib/geoclue-2.0/demos/where-am-i",
    -- "ibus-daemon -drxR",
    "~/.local/share/JetBrains/Toolbox/bin/jetbrains-toolbox --minimize",
    "nm-applet",
    "pcloud",
    "picom -b --config ~/.config/awesome/picom/picom.conf",
    "redshift-gtk",
}

local function run_once(command)
    local first_space = command:find(" ")
    local basename = command
    -- command --optionとなっている場合--optionを取り除く
    if first_space then
        basename = command:sub(0, first_space - 1)
    end

    awful.spawn.with_shell("pgrep -u $USER -i '" .. basename .. "' > /dev/null || (" .. command .. ")")
end

function autostart.run_commands()
    for _, cmd in pairs(autostart.commands) do
        run_once(cmd)
    end
end

-- function autostart.run_desktops()
--     -- ~/.config/autostart/*.desktopを自動的に起動する処理
-- end

function autostart.run()
    autostart.run_commands()
    -- autostart.ren_desktops()
end

return autostart
