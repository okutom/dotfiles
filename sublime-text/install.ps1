If(!(Test-Path "$Env:PROGRAMFILES/Sublime Text/sublime_text.exe")) {
    Write-Host -ForegroundColor Red "Sublime Text is not installed."
    exit 1
}

$script_dir = Split-Path $MyInvocation.MyCommand.Path
$sublime_user_dir = "$Env:AppData/Sublime Text/Packages/User"

If(Test-Path "$sublime_user_dir") {
    Remove-Item -Path "$sublime_user_dir" -Recurse -Force
}

New-Item -ItemType SymbolicLink -Value "$script_dir" "$sublime_user_dir" -Force
New-Item -ItemType SymbolicLink -Value "$script_dir/../vim/vimscript/astarte.vim" "$sublime_user_dir/.neovintageousrc"
