return {
    'nvim-orgmode/orgmode',
    enabled = false,
    dependencies = {
        { 'nvim-treesitter/nvim-treesitter', lazy = true },
    },
    event = 'VeryLazy',
    config = function()
        require('orgmode').setup_ts_grammar()
        require('nvim-treesitter.configs').setup({
            highlight = {
                enable = true,
            },
            ensure_installed = { 'org' },
        })
        require('orgmode').setup({
            org_agenda_files = '~/org/**/*',
            org_default_notes_file = '~/org/Quieck Note.org'
        })
    end,
}
