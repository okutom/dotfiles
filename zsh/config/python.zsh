#!/usr/bin/env zsh

export PYENV_ROOT="${HOME}/.pyenv"
if [[ -e "${PYENV_ROOT}" ]]; then
  export PATH="${PYENV_ROOT}/bin:${PATH}"
  if type pyenv &>/dev/null; then
    eval "$(pyenv init -)"
  fi
else
  unset PYENV_ROOT
fi
