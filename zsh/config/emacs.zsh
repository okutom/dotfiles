#!/usr/bin/env zsh

if type emacs &>/dev/null; then
  function estart() {
    if ! emacsclient -e 0 &>/dev/null; then
      cd > /dev/null 2>&1
      emacs --daemon &>/dev/null
      cd - &>/dev/null
    fi
  }

  estart
  alias eclient='emacsclient -c -a emacs'
  alias ekill='emacsclient -e "(kill-emacs)"'
  alias erestart='emacsclient -e "(kill-emacs)" && estart'
fi
