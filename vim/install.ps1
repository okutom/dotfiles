If(!(Get-Command -Name nvim -ErrorAction SilentlyContinue)) {
    Write-Host -ForegroundColor Red "neovim is not installed."
    exit 1
}

$script_dir = Split-Path $MyInvocation.MyCommand.Path

New-Item -ItemType SymbolicLink -Value "$script_dir" "$env:localappdata/nvim" -Force