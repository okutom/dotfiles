#!/usr/bin/env

if [[ -e /mnt/c ]]; then
    export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2}'):0
fi
