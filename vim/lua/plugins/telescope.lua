return {
    'nvim-telescope/telescope.nvim',
    lazy = true,
    event = 'VeryLazy',
    dependencies = {
        'nvim-lua/plenary.nvim',
        'nvim-telescope/telescope-fzy-native.nvim',
        'nvim-telescope/telescope-live-grep-args.nvim',
        {
            'nvim-telescope/telescope-dap.nvim',
            dependencies = {
                'mfussenegger/nvim-dap',
                'nvim-treesitter/nvim-treesitter',
            },
        },
        'debugloop/telescope-undo.nvim',
        {
            'ahmedkhalf/project.nvim',
            event = 'VeryLazy',
            main = 'project_nvim',
            opts = {},
        },
    },
    config = function()
        local actions = require('telescope.actions')
        require('telescope').setup({
            defaults = {
                layout_strategy = 'horizontal',
                layout_config = {
                    horizontal = {
                        prompt_position = 'top',
                        prompt_width = 'half',
                        results_width = 'half',
                    },
                },
                sorting_strategy = 'ascending',
                mappings = {
                    i = {
                        ['<C-t>'] = actions.move_selection_next,
                        ['<C-n>'] = actions.move_selection_previous,

                        ['<C-k>'] = actions.results_scrolling_left,
                        ['<C-s>'] = actions.results_scrolling_right,
                        ['<PageDown>'] = actions.results_scrolling_down,
                        ['<PageUp>'] = actions.results_scrolling_up,

                        ['<A-k>'] = actions.preview_scrolling_left,
                        ['<A-t>'] = actions.preview_scrolling_down,
                        ['<A-n>'] = actions.preview_scrolling_up,
                        ['<A-s>'] = actions.preview_scrolling_right,
                    },
                    n = {
                        ['k'] = actions.results_scrolling_left,
                        ['t'] = actions.move_selection_next,
                        ['n'] = actions.move_selection_previous,
                        ['s'] = actions.results_scrolling_right,
                    },
                },
            },
            extensions = {
                fzy_native = {
                    override_generic_sorter = false,
                    override_file_sorter = true,
                },
                live_grep_args = {
                    auto_quoting = true,
                },
                undo = {
                    side_by_side = true,
                },
            },
        })
        require('telescope').load_extension('dap')
        require('telescope').load_extension('fzy_native')
        require('telescope').load_extension('live_grep_args')
        require('telescope').load_extension('projects')
        require('telescope').load_extension('undo')
    end,
    keys = {
        { mode = { 'n' }, '<leader>tc', '<cmd>Telescope commands<cr>', desc = 'Commands' },
        { mode = { 'n' }, '<leader>tb', '<cmd>Telescope buffers<cr>', desc = 'Buffers' },

        { mode = { 'n' }, '<leader>tff', '<cmd>Telescope find_files<cr>', desc = 'Find files' },
        { mode = { 'n' }, '<leader>tfg', '<cmd>Telescope git_files<cr>', desc = 'Git files' },
        { mode = { 'n' }, '<leader>tfG', '<cmd>Telescope grep_string<cr>', desc = 'Grep string' },
        { mode = { 'n' }, '<leader>tfl', '<cmd>Telescope live_grep<cr>', desc = 'Live grep' },
        { mode = { 'n' }, '<leader>tfL', '<cmd>Telescope live_grep_args<cr>', desc = 'Live grep (args)' },

        { mode = { 'n' }, '<leader>tgb', '<cmd>Telescope git_branches<cr>', desc = 'Branches' },
        { mode = { 'n' }, '<leader>tgc', '<cmd>Telescope git_commits<cr>', desc = 'Commits' },
        { mode = { 'n' }, '<leader>tgC', '<cmd>Telescope git_bcommits<cr>', desc = 'Commits (current buffer)' },
        { mode = { 'n' }, '<leader>tgs', '<cmd>Telescope git_status<cr>', desc = 'Status' },
        { mode = { 'n' }, '<leader>tgS', '<cmd>Telescope git_stash<cr>', desc = 'Stash' },

        { mode = { 'n' }, '<leader>th', '<cmd>Telescope help_tags<cr>', desc = 'Help tags' },
        { mode = { 'n' }, '<leader>tk', '<cmd>Telescope keymaps<cr>', desc = 'Keymaps' },

        { mode = { 'n' }, '<leader>tlb', '<cmd>Telescope lsp_document_symbols<cr>', desc = 'lists symbols in current Buffer' },
        { mode = { 'n' }, '<leader>tlc', '<cmd>Telescope lsp_incoming_calls<cr>', desc = 'incoming Calls' },
        { mode = { 'n' }, '<leader>tlC', '<cmd>Telescope lsp_outgoing_calls<cr>', desc = 'outgoing Calls' },
        { mode = { 'n' }, '<leader>tld', '<cmd>Telescope diagnostics<cr>', desc = 'Diagnostics' },
        { mode = { 'n' }, '<leader>tlD', '<cmd>Telescope lsp_definitions<cr>', desc = 'Definitions' },
        { mode = { 'n' }, '<leader>tli', '<cmd>Telescope lsp_implementations<cr>', desc = 'Implementations' },
        { mode = { 'n' }, '<leader>tlr', '<cmd>Telescope lsp_references<cr>', desc = 'References' },
        { mode = { 'n' }, '<leader>tlt', '<cmd>Telescope lsp_type_definitions<cr>', desc = 'Type definitions' },
        { mode = { 'n' }, '<leader>tlw', '<cmd>Telescope lsp_workspace_symbols<cr>', desc = 'lists symbols in current Workspace' },
        { mode = { 'n' }, '<leader>tlW', '<cmd>Telescope lsp_dynamic_workspace_symbols<cr>', desc = 'dynamically lists symbols for all Workspace' },

        { mode = { 'n' }, '<leader>tm', '<cmd>Telescope marks<cr>', desc = 'Marks' },
        { mode = { 'n' }, '<leader>tt', '<cmd>Telescope tags<cr>', desc = 'Tags' },
        { mode = { 'n' }, '<leader>tp', '<cmd>Telescope projects<cr>', desc = 'Projects' },
        { mode = { 'n' }, '<leader>tu', '<cmd>Telescope undo<cr>', desc = 'undo history' },
        { mode = { 'n' }, '<leader>tq', '<cmd>Telescope quickfix<cr>', desc = 'Quickfix' },
    }
}
