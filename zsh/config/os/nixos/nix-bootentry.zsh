#!/usr/bin/env zsh

nix-bootentry() {
  help() {
    echo 'Usage: nix-bootentry command [option]'
    echo '  help   show this help'
    echo '  show   show boot entries'
    echo '  delete delete boot entries'
  }

  show-boot-entries() {
    sudo nix-env --list-generations --profile /nix/var/nix/profiles/system
  }

  delete-boot-entries() {
    if [[ $# -eq 0 ]]; then
      sudo nix-env --delete-generations --profile /nix/var/nix/profiles/system 7d
    else
      sudo nix-env --delete-generations --profile /nix/var/nix/profiles/system $1
    fi
  }

  unset-function() {
    unset -f help
    unset -f show-boot-entries
    unset -f delete-boot-entries
  }

  for arg in $*
  do
    case $arg in
      help)
        help
        unset-function
        unset -f unset-function
        return
        ;;
      show)
        show-boot-entries
        unset-function
        unset -f unset-function
        return
        ;;
      delete)
        delete-boot-entries "${@:2}"
        unset-futciton
        unset -f unset-function
        return
        ;;
    esac
  done
  # ここに到達したら何も引数を指定していない
  help
  unset-function
  unset -f unset-function
}

