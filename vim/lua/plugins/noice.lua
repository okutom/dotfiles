return {
    'folke/noice.nvim',
    dependencies = {
        'MunifTanjim/nui.nvim',
        {
            'rcarriga/nvim-notify',
            lazy = true,
            event = 'VeryLazy',
            opts = {
                max_width = 40,
                stages = 'slide',
                render = 'wrapped-compact',
            },
        },
    },
    lazy = true,
    event = 'VeryLazy',
    opts = {
        lsp = {
            override = {
                ['vim.lsp.util.convert_input_to_markdown_lines'] = true,
                ['vim.lsp.util.stylize_markdown'] = true,
                ['cmp.entry.get_documentation'] = true,
            },
            signature = {
                enabled = false,
            },
        },
        presets = {
            bottom_search = true,
            command_palette = true,
            long_message_to_split = true,
            inc_rename = false,
            lsp_doc_border = false,
        },
        routes = {
            {
                filter = { event = 'notify', find = 'Config Change Detected. Reloading...' },
                view = 'mini',
            },
        },
    },
}
