#!/usr/bin/env zsh

alias update='sudo dnf update'
alias autoremove='sudo dnf autoremove'
alias listfile='dnf repoquery -l'
