local utils = {
    file_exists = function(filename)
        local f = io.open(filename, "r")
        if f ~= nil then
            io.close(f)
            return true
        else
            return false
        end
    end,

    set_wallpaper = function(s)
        local beautiful = require("beautiful")
        local gears = require("gears")
        if beautiful.wallpaper then
            local wallpaper = beautiful.wallpaper
            -- If wallpaper is a function, call it with the screen
            if type(wallpaper) == "function" then
                wallpaper = wallpaper(s)
            end
            gears.wallpaper.maximized(wallpaper, s, true)
        end
    end
}

return utils
