#!/usr/bin/env bash

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)
local_bin="${HOME}/.local/bin"

mkdir -p "${local_bin}"
for file in "${abs_script_dir}"/??*; do
    if [ "${file##*/}" = $(basename "${0}") ]; then
    continue
  fi
  if [ "${file##*/}" = "ssh-add.sh" ]; then
    if [ ! -e "${local_bin}/${file##*/}" ];then
      cp "${file}" "${local_bin}/${file##*/}"
    fi
    continue
  fi
  ln -snfv "${file}" "${local_bin}/${file##*/}"
done

unset abs_script_dir
unset script_dir
