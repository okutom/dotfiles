return {
    'nvim-neo-tree/neo-tree.nvim',
    branch = 'v3.x',
    lazy = true,
    event = 'VeryLazy',
    dependencies = {
        'nvim-lua/plenary.nvim',
        'nvim-tree/nvim-web-devicons',
        'MunifTanjim/nui.nvim',
        {
            's1n7ax/nvim-window-picker',
            version = '2.*',
            config = function()
                require('window-picker').setup({
                    hint = 'floating-big-letter',
                    selection_chars = 'ATENOSIRPDUHYGQ;',
                    filter_rules = {
                        bo = {
                            filetype = { 'neo-tree', 'neo-tree-popup', 'notify' },
                            buftype = { 'terminal', 'quickfix' },
                        },
                    },
                })
            end,
        },
    },
    config = function()
        vim.fn.sign_define("DiagnosticSignError", { text = " ", texthl = "DiagnosticSignError" })
        vim.fn.sign_define("DiagnosticSignWarn", { text = " ", texthl = "DiagnosticSignWarn" })
        vim.fn.sign_define("DiagnosticSignInfo", { text = " ", texthl = "DiagnosticSignInfo" })
        vim.fn.sign_define("DiagnosticSignHint", { text = "󰌵", texthl = "DiagnosticSignHint" })

        require('neo-tree').setup({
            source_selector = {
                winbar = true,
                sources = {
                    { source = "filesystem", display_name = " 󰉋 File " },
                    { source = "buffers", display_name = "  Buff " },
                    { source = "git_status", display_name = " 󰊢 Git " },
                },
            },
            window = {
                width = 30,
                mappings = {
                    ['t'] = false,
                    ['s'] = false,
                    ['S'] = false,
                },
            },
        })
    end,
    cmd = { 'Neotree' },
    keys = {
        {
            mode = { 'n', 'i' },
            '<A-1>',
            function()
                local action = 'filesystem'
                local buf_name = vim.api.nvim_buf_get_name(0)
                local file_type = vim.api.nvim_buf_get_option(0, 'filetype')
                local r = buf_name:match(action)
                if file_type == 'neo-tree' and r ~= nil then
                    vim.api.nvim_exec('Neotree close', true)
                else
                    vim.api.nvim_exec('Neotree focus ' .. action, true)
                end
            end,
            noremap = true,
            desc = 'toggle file explorer',
        },
        {
            mode = { 'n', 'i' },
            '<A-2>',
            function()
                local action = 'buffers'
                local buf_name = vim.api.nvim_buf_get_name(0)
                local file_type = vim.api.nvim_buf_get_option(0, 'filetype')
                local r = buf_name:match(action)
                if file_type == 'neo-tree' and r ~= nil then
                    vim.api.nvim_exec('Neotree close', true)
                else
                    vim.api.nvim_exec('Neotree focus ' .. action, true)
                end
            end,
            noremap = true,
            desc = 'toggle buffer list',
        },
        {
            mode = { 'n', 'i' },
            '<A-3>',
            function()
                local action = 'git_status'
                local buf_name = vim.api.nvim_buf_get_name(0)
                local file_type = vim.api.nvim_buf_get_option(0, 'filetype')
                local r = buf_name:match(action)
                if file_type == 'neo-tree' and r ~= nil then
                    vim.api.nvim_exec('Neotree close', true)
                else
                    vim.api.nvim_exec('Neotree focus ' .. action, true)
                end
            end,
            noremap = true,
            desc = 'toggle git status',
        },
    },
}
