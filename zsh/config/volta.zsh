#!/usr/bin/env zsh

tmp_volta_home="${HOME}/.volta"

if [[ -e "${tmp_volta_home}" ]]; then
  export VOLTA_HOME="${tmp_volta_home}"
  export PATH="${VOLTA_HOME}/bin:$PATH"
fi

unset tmp_volta_home

