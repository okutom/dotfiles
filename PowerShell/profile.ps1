# Powershellのフォルダを定義しておく
$pwsh_dir = [Environment]::GetFolderPath("MyDocuments") + "/PowerShell"

# 補完をzsh風に
Set-PSReadLineKeyHandler -Key Tab -Function MenuComplete

# sshをシステム標準のを使うようにする
$env:GIT_SSH = "C:/Windows/System32/OpenSSH/ssh.exe"

# 自作スクリプトのパスを追加
$env:Path += ";$HOME/Documents/PowerShell/Scripts/"

# モジュールをインポート
Import-Module Get-PID

# Command Not Found by PowerToys
If(Test-Path "$env:LOCALAPPDATA\PowerToys\WinGetCommandNotFound.psd1") {
    If (!(Get-Module -ListAvailable -Name Microsoft.WinGet.Client)) {
        Install-Module -Force -Confirm:$false Microsoft.WinGet.Client
    }
    Import-Module "$env:LOCALAPPDATA\PowerToys\WinGetCommandNotFound.psd1"
}

# voltaタブ補完
If(Get-Command volta -ea SilentlyContinue) {
    (& volta completions powershell) | Out-String | Invoke-Expression
}

# omp読み込み
# oh-my-posh prompt init pwsh --config "$pwsh_dir/paradox_monokai.omp.json" | Invoke-Expression

# starship読み込み
$ENV:STARSHIP_CONFIG = "$pwsh_dir/starship/starship-nf.toml"
Invoke-Expression (&starship init powershell)
