return {
    'kevinhwang91/nvim-bqf',
    dependenceis = {
        'nvim-treesitter/nvim-treesitter',
        build = ':TSUpdate',
    },
    lazy = true,
    ft = 'qf',
}
