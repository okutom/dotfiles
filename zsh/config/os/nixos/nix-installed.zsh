#!/usr/bin/env zsh

nix-installed() {
  help() {
    echo 'Usage: nix-installed option [package]'
    echo '  -h --help    show this help'
    echo '  -c --config  search package installed via configuration.nix'
    echo '  -d --dep     search dependencies of the current system'
    echo '  -u --user    search package installed in user environment'
    return
  }

  search-via-config() {
    if [[ $# -eq 0 ]]; then
      nixos-option environment.systemPackages
    else
      nixos-option environment.systemPackages | grep $1
    fi
  }

  search-with-dep() {
    if [[ $# -eq 0 ]]; then
      nix-store --query --requisites /run/current-system | cut -d- -f2- | sort | uniq
    else
      nix-store --query --requisites /run/current-system | cut -d- -f2- | sort | uniq | grep $1
    fi
  }

  search-by-user() {
    if [[ $# -eq 0 ]]; then
      nix-env --query
    else
      nix-env --query | grep $1
    fi
  }

  unset-function() {
    unset -f help
    unset -f search-via-config
    unset -f search-with-dep
    unset -f search-by-user
  }

  for arg in $*
  do
    case $arg in
      -h | --help)
        help
        unset-function
        unset -f unset-function
        return
        ;;
      -c | --config)
        search-via-config "${@:2}"
        unset-function
        unset -f unset-function
        return
        ;;
      -d | --dep)
        search-with-dep "${@:2}"
        unset-function
        unset -f unset-function
        return
        ;;
      -u | --user)
        search-by-user "${@:2}"
        unset-function
        unset -f unset-function
        return
        ;;
      *)
        help
        unset-function
        unset -f unset-function
        return
        ;;
    esac
  done
  # ここに到達したら何も引数を指定していない
  help
  unset-function
  unset -f unset-function
}

