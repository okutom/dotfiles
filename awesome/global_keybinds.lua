local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")
local gears = require("gears")
local menu = require("menu")
local terminal = require("terminal")
local audio = require("widget.audio")
local logout = require("widget.logout")

local global_keybinds = {}

global_keybinds.keys = gears.table.join(
    awful.key(
        { "Mod4" }, "space",
        function()
            local s = awful.screen.focused()
            s.wb.visible = not s.wb.visible
        end,
        { description = "toggle taskbar", group = "screen" }
    ),
    awful.key(
        { "Mod4", "Control", "Shift" }, "r",
        function() logout:refresh() end,
        { description = "reload awesome", group = "launcher" }
    ),
    awful.key(
        { "Mod4" }, "h",
        hotkeys_popup.show_help,
        { description = "show help", group = "awesome" }
    ),
    awful.key(
        { "Mod4" }, "Left",
        awful.tag.viewprev,
        { description = "view previous", group = "tag" }
    ),
    awful.key(
        { "Mod4" }, "Right",
        awful.tag.viewnext,
        { description = "view next", group = "tag" }
    ),
    awful.key(
        { "Mod1" }, "Tab",
        function() awful.client.focus.byidx(1) end,
        { description = "move focus to next", group = "client" }
    ),
    awful.key(
        { "Mod1", "Shift" }, "Tab",
        function() awful.client.focus.byidx(-1) end,
        { description = "move focus to previous", group = "client" }
    ),
    awful.key(
        { "Mod4" }, "]",
        function() awful.screen.focus_relative(1) end,
        { description = "move focus to next screen", group = "screen" }
    ),
    awful.key(
        { "Mod4", }, "[",
        function() awful.screen.focus_relative(-1) end,
        { description = "move focus to previous screen", group = "screen" }
    ),
    awful.key(
        { "Mod4" }, "Escape",
        awful.client.urgent.jumpto,
        { description = "jump to urgent client", group = "client" }
    ),
    awful.key(
        { "Mod4", "Shift" }, "m",
        function()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal("request::activate", "key.unminimize", {raise = true})
            end
        end,
        { description = "restore minimized", group = "client" }
    ),

    -- Move focus
    awful.key(
        { "Mod4" }, "k",
        function() awful.client.focus.bydirection("left") end,
        { description = "move focus to left", group = "client" }
    ),
    awful.key(
        { "Mod4" }, "t",
        function() awful.client.focus.bydirection("down") end,
        { description = "move focus to down", group = "client" }
    ),
    awful.key(
        { "Mod4" }, "n",
        function() awful.client.focus.bydirection("up") end,
        { description = "move focus to up", group = "client" }
    ),
    awful.key(
        { "Mod4" }, "s",
        function() awful.client.focus.bydirection("right") end,
        { description = "move focus to right", group = "client"}
    ),

    -- Swap direction
    awful.key(
        { "Mod4", "Control" }, "k",
        function() awful.client.swap.bydirection("left") end,
        { description = "swap with left client", group = "client" }
    ),
    awful.key(
        { "Mod4", "Control" }, "t",
        function() awful.client.swap.bydirection("down") end,
        { description = "swap with down client", group = "client" }
    ),
    awful.key(
        { "Mod4", "Control" }, "n",
        function() awful.client.swap.bydirection("up") end,
        { description = "swap with up client", group = "client" }
    ),
    awful.key(
        { "Mod4", "Control" }, "s",
        function() awful.client.swap.bydirection("right") end,
        { description = "swap with right client", group = "client" }
    ),
    -- Resize client
    awful.key(
        { "Mod4", "Shift" }, "k",
        function() awful.tag.incmwfact(-0.01) end,
        { description = "decrease client width", group = "layout" }
    ),
    awful.key(
        { "Mod4", "Shift" }, "t",
        function() awful.client.incwfact(0.01) end,
        { description = "increase client height", group = "layout" }
    ),
    awful.key(
        { "Mod4", "Shift" }, "n",
        function() awful.client.incwfact(-0.01) end,
        { description = "decrease client height", group = "layout" }
    ),
    awful.key(
        { "Mod4", "Shift" }, "s",
        function() awful.tag.incmwfact(0.01) end,
        { description = "increase client width", group = "layout" }
    ),

    -- Switch layout
    awful.key(
        { "Mod4" }, "l",
        function() awful.layout.inc(1) end,
        { description = "select next", group = "layout" }
    ),
    awful.key(
        { "Mod4", "Shift" }, "l",
        function() awful.layout.inc(-1) end,
        { description = "select previous", group = "layout" }
    ),

    -- Screenshot
    awful.key({ }, "Print",
        function() awful.spawn.with_shell(gears.filesystem.get_configuration_dir() .. "/script/screenshot.zsh") end,
        { description = "take a screenshot (all display)", group = "launcher" }
    ),
    awful.key(
        { "Mod1" }, "Print",
        function() awful.spawn.with_shell(gears.filesystem.get_configuration_dir() .. "/script/screenshot.zsh -w") end,
        { description = "take a screenshot (focused window)", group = "launcher" }
    ),
    awful.key(
        { "Shift" }, "Print",
        function() awful.spawn.with_shell(gears.filesystem.get_configuration_dir() .. "/script/screenshot.zsh -a") end,
        { description = "take a screenshot (select area)", group = "launcher" }
    ),

    -- Audio
    awful.key(
        { }, "XF86AudioRaiseVolume",
        function() audio:inc(1) end,
        { description = "volume up", group = "HW control" }
    ),
    awful.key(
        { }, "XF86AudioLowerVolume",
        function() audio:dec(1) end,
        { description = "volume down", group = "HW control" }
    ),
    awful.key(
        { }, "XF86AudioMute",
        function() audio:toggle() end,
        { description = "(un)mute", group = "HW control" }
    ),

    -- Brightness
    awful.key(
        { }, "XF86MonBrightnessUp",
        function() end,
        {description = "increase brightness(NOT IMPL NOW)", group = "HW control"}
    ),
    awful.key(
        { }, "XF86MonBrightnessDown",
        function() end,
        {description = "decrease brightness(NOT IMPL NOW)", group = "HW control"}
    ),

    -- Launcher
    awful.key(
        { "Mod4" }, "x",
        function() awful.spawn.with_shell("rofi -show") end,
        { description = "show rofi", group = "launcher" }
    ),
    awful.key(
        { "Mod4" }, "l",
        function() end,
        { description = "lock screen(NOT IMPL NOW)", group = "launcher" }
    ),
    awful.key(
        { "Mod4" }, "Return",
        function() awful.spawn(terminal) end,
        { description = "open a terminal", group = "launcher" }
    ),
    awful.key(
        { "Control", "Shift" }, "Escape",
        function() awful.spawn.with_shell(terminal .. " -e htop") end,
        { description = "show task manager", group = "launcher" }
    ),
    awful.key(
        { "Mod4" }, "p",
        function() end,
        { description = "quick display settings(NOT IMPL NOW)", group = "launcher" }
    ),
    awful.key(
        { "Mod4" }, "e",
        function() awful.spawn("nautilus") end,
        { description = "launch file manager", group = "launcher" }
    ),
    awful.key(
        { "Mod4" }, ".",
        function() awful.spawn.raise_or_spawn("emote") end,
        { description = "show emoji picker", group = "launcher" }
    )
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    global_keybinds.keys = gears.table.join(global_keybinds.keys,
        -- View tag only
        awful.key(
            { "Mod4" }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    tag:view_only()
                end
            end,
            { description = "view tag #" .. i, group = "tag" }
        ),
        -- Toggle tag display.
        awful.key(
            { "Mod4", "Control" }, "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            { description = "toggle tag #" .. i, group = "tag" }
        ),
        -- Move client to tag.
        awful.key(
            { "Mod4", "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end,
            { description = "move focused client to tag #" .. i, group = "tag" }
        ),
        -- Toggle tag on focus client.
        awful.key(
            { "Mod4", "Control", "Shift" }, "#" .. i + 9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            { description = "toggle focused client on tag #" .. i, group = "tag" }
        )
    )
end

global_keybinds.buttons = gears.table.join(
    awful.button({ }, 3, function() menu.context_menu:toggle() end),
    awful.button({ }, 4, awful.tag.viewprev),
    awful.button({ }, 5, awful.tag.viewnext)
)

global_keybinds.tag_buttons = gears.table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ "Control" }, 1, awful.tag.viewtoggle),
    awful.button(
        { "Shift" }, 1,
        function(t)
            if client.focus then
                client.focus:move_to_tag(t)
            end
        end),
    awful.button(
        { }, 3,
        function(t)
            if client.focus then
                client.focus:toggle_tag(t)
            end
        end),
    awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
    awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end)
)

global_keybinds.task_buttons = gears.table.join(
    awful.button(
        { }, 1,
        function(c)
            if c == client.focus then
                c.minimized = true
            else
                c:emit_signal(
                    "request::activate",
                    "tasklist",
                    { raise = true }
                )
            end
        end
    ),
    awful.button({ }, 3, function() awful.menu.client_list({ theme = { width = 250 } }) end),
    awful.button({ }, 4, function() awful.client.focus.byidx(-1) end),
    awful.button({ }, 5, function() awful.client.focus.byidx(1) end)
)

global_keybinds.layout_buttons = gears.table.join(
    awful.button({ }, 1, function() awful.layout.inc(1) end),
    awful.button({ }, 3, function() awful.layout.inc(-1) end),
    awful.button({ }, 4, function() awful.layout.inc(-1) end),
    awful.button({ }, 5, function() awful.layout.inc(1) end)
)

return global_keybinds
