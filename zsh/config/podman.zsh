#!/usr/bin/env zsh

if type podman &>/dev/null; then
  alias docker='podman'
fi
