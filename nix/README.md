# NixOS Configurations

## System configuration

### Install

```sh
$ cd /path/to/dotfiles/nix
# apply the result at next boot
$ ./nix-setup.bash system boot
or
# apply the result now
$ ./nix-setup.bash system switch
```

### Update

```sh
$ ./nix-setup.bash system update
```

## User app configuration

### Install

```sh
$ ./nix-setup.bash home install
```

### Update

```sh
$ ./nix-setup.bash home update
```
