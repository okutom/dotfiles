$script_dir = Split-Path $MyInvocation.MyCommand.Path

$pwsh_dir = [Environment]::GetFolderPath("MyDocuments") + "/PowerShell"

$items_in_dir = Get-ChildItem -Path "$script_dir" -Name
foreach($item in $items_in_dir) {
    if($item -eq "Modules") {
        New-Item -ItemType Directory -Path "$pwsh_dir/Modules" -Force
        $items_in_modules = Get-ChildItem -Path "$script_dir/Modules" -Name
        foreach($modules in $items_in_modules) {
            New-Item -ItemType SymbolicLink -Value "$script_dir/Modules/$modules" -Path "$pwsh_dir/Modules/$modules" -Force
        }
    } else {
        New-Item -ItemType SymbolicLink -Value "$script_dir/$item" "$pwsh_dir/$item" -Force
    }
}