[Appearance]
ColorScheme=Monokai
Font=UDEV Gothic NFLG,11,-1,5,50,0,0,0,0,0
UseFontLineChararacters=true

[General]
Name=Default
Parent=FALLBACK/
ShowTerminalSizeHint=false
TerminalMargin=0

[Scrolling]
ScrollBarPosition=2
