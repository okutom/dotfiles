#!/usr/bin/env zsh

temp_dir="${HOME}/.local/lib/azure-cli"

if [[ -e "${temp_dir}" ]]; then
  autoload -U +X bashcompinit && bashcompinit
  source "${temp_dir}/az.completion"
fi

unset temp_dir
