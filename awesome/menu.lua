local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")
local beautiful = require("beautiful")
local terminal = require("terminal")
local logout = require("widget.logout")

local menu = {}

menu.context_menu = awful.menu({
    items = {
        { "Keymap", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end, beautiful.keymap },
        { "Open Terminal", terminal, beautiful.terminal },
        { "Refresh", function() logout:refresh() end, beautiful.refresh },
        { "Logout", function() logout:logout() end, beautiful.logout },
        { "Sleep", function() logout:sleep() end, beautiful.sleep },
        { "Reboot", function() logout:reboot() end, beautiful.reboot },
        { "Shutdown", function() logout:shutdown() end, beautiful.shutdown },
    }
})

return menu
