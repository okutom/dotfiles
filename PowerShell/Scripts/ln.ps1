Param(
    [string]$source,
    [string]$target,
    [switch]$s,
    [switch]$f,
    [switch]$i,
    [switch]$v
)

Function Is-Admin() {
    $currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
    Return $currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
}

$is_dir = (Get-Item -Force "$source").PSIsContainer
$type = "HardLink"
If($is_dir) {
    If($s) {
        If(Is-Admin) {
            $type = "SymbolicLink"
        } Else {
            $type = "Junction"
        }
    }
} Else {
    If($s) {
        $type = "SymbolicLink"
    }
}

If($v) {
    If($f -and $i) {
        New-Item -ItemType $type -Value "$source" "$target" -Force -Confirm
    } ElseIf($f) {
        New-Item -ItemType $type -Value "$source" "$target" -Force
    } ElseIf($i) {
        New-Item -ItemType $type -Value "$source" "$target" -Confirm
    } Else {
        New-Item -ItemType $type -Value "$source" "$target"
    }
} Else {
    If($f -and $i) {
        New-Item -ItemType $type -Value "$source" "$target" -Force -Confirm > $null
    } ElseIf($f) {
        New-Item -ItemType $type -Value "$source" "$target" -Force > $null
    } ElseIf($i) {
        New-Item -ItemType $type -Value "$source" "$target" -Confirm > $null
    } Else {
        New-Item -ItemType $type -Value "$source" "$target" > $null
    }
}