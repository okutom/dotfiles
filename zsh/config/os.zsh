#!/usr/bin/env zsh

local config_dir="${ZDOTDIR}/config"

source /etc/os-release

local -a id_like=(`echo "${ID_LIKE}"`)
local -a target=()

target+=("${ID}")
for i in "${id_like}"; do
  target+=("${i}")
done

if [[ "${OSTYPE}" == linux* ]]; then
  target+=(linux-common)
else
  target+=(bsd-common)
fi

for i in "${target[@]}"; do
  if [[ -e "${config_dir}/os/${i}.zsh" ]]; then
    source "${config_dir}/os/${i}.zsh"
  fi
done

