$script_dir = Split-Path $MyInvocation.MyCommand.Path
$dir_list = Get-ChildItem -Path "$script_dir" -Directory

foreach($dir in $dir_list) {
    $item_list = Get-ChildItem -Path "$dir/*" -Include *.ps1
    foreach($item in $item_list) {
        if($item.Name -eq "install.ps1") {
            Invoke-Expression $item
        }
    }
}
