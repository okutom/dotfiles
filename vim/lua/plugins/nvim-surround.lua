return {
    'kylechui/nvim-surround',
    lazy = true,
    event = 'VeryLazy',
    dependencies = {
        'nvim-treesitter/nvim-treesitter',
        'nvim-treesitter/nvim-treesitter-textobjects',
    },
    opts = {},
}
