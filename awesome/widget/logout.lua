-- https://github.com/streetturtle/awesome-wm-widgets/tree/master/logout-menu-widget
-- https://github.com/streetturtle/awesome-wm-widgets/blob/master/LICENSE
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")

-- load my theme
beautiful.init(gears.filesystem.get_configuration_dir() .. "/theme/theme.lua")

local logout = {}

logout.widget = wibox.widget {
    {
        {
            image = beautiful.shutdown,
            resize = true,
            widget = wibox.widget.imagebox,
        },
        margins = 4,
        layout = wibox.container.margin
    },
    shape = function(cr, width, height)
        gears.shape.rectangle(cr, width, height)
    end,
    widget = wibox.container.background,
}

logout.popup = awful.popup {
    ontop = true,
    visible = false,
    shape = function(cr, width, height)
        gears.shape.rectangle(cr, width, height)
    end,
    border_width = beautiful.border_width,
    border_color = beautiful.border_focus,
    --maximum_width = 400,
    widget = {}
}

local function worker()
    local rows = { layout = wibox.layout.fixed.vertical }

    function logout:refresh()
        awesome.restart()
    end
    function logout:logout()
        awesome.quit()
        local session = os.getenv("DESKTOP_SESSION")
        if string.find(string.lower(session), "gnome") or string.find(string.lower(session), "pop") then
            awful.spawn.with_shell("killall xss-lock")
            awful.spawn.with_shell("gnome-session-quit --logout --no-prompt")
        end
    end
    function logout:sleep()
        -- awful.spawn.with_shell("systemctl hybrid-sleep")
        awful.spawn.with_shell("systemctl suspend-then-hibernate")
    end
    function logout:reboot()
        awful.spawn.with_shell("systemctl reboot")
    end
    function logout:shutdown()
        awful.spawn.with_shell("systemctl poweroff")
    end

    local menu_items =  {
        { name = "Refresh", command = function() logout:refresh() end, icon = beautiful.refresh },
        { name = "Logout", command = function() logout:logout() end, icon = beautiful.logout },
        { name = "Sleep", command = function() logout:sleep() end, icon = beautiful.sleep },
        { name = "Reboot", command = function() logout:reboot() end, icon = beautiful.reboot },
        { name = "Shutdown", command = function() logout:shutdown() end, icon = beautiful.shutdown },
    }

    for _, item in ipairs(menu_items) do
        local row = wibox.widget {
            {
                {
                    {
                        {
                            image = item.icon,
                            resize = true,
                            widget = wibox.widget.imagebox
                        },
                        width = 24,
                        height = 24,
                        layout = wibox.container.constraint
                    },
                    {
                        text = item.name,
                        font = beautiful.font_name .. '16',
                        widget = wibox.widget.textbox
                    },
                    spacing = 5,
                    layout = wibox.layout.fixed.horizontal
                },
                margins = 5,
                layout = wibox.container.margin
            },
            bg = beautiful.bg_normal,
            widget = wibox.container.background
        }

        row:connect_signal("mouse::enter", function(c) c:set_bg(beautiful.bg_focus) end)
        row:connect_signal("mouse::leave", function(c) c:set_bg(beautiful.bg_normal) end)

        local old_cursor, old_wibox
        row:connect_signal("mouse::enter", function()
            local wb = mouse.current_wibox
            old_cursor, old_wibox = wb.cursor, wb
            wb.cursor = "hand1"
        end)
        row:connect_signal("mouse::leave", function()
            if old_wibox then
                old_wibox.cursor = old_cursor
                old_wibox = nil
            end
        end)

        row:buttons(awful.util.table.join(awful.button({}, 1, function()
            logout.popup.visible = not logout.popup.visible
            item.command()
        end)))

        table.insert(rows, row)
    end

    logout.popup:setup(rows)
    logout.widget:buttons(
        awful.util.table.join(
            awful.button({}, 1, function()
                if logout.popup.visible then
                    logout.popup.visible = not logout.popup.visible
                    logout.widget:set_bg(beautiful.bg_normal)
                else
                    logout.popup:move_next_to(mouse.current_widget_geometry)
                    logout.widget:set_bg(beautiful.bg_focus)
                end
            end)
        )
    )

    return logout.widget
end

logout.applet = setmetatable(logout.widget, { __call = function(_, ...) return worker() end })

return logout
