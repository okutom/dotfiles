{ config, lib, pkgs, ... }:
let
  hostName = "nixos";
  userName = "okutom";
  hashedPassword = "$y$j9T$.oH8lnMi/ZM/uFtv2g.r81$eWN5EFPuvPkryYMc7X56NK9ZKfZocaOqtEf31ORwmt.";
in
{
  boot = {
    loader = {
      systemd-boot = {
        enable = true;
        editor = false;
      };
      efi.canTouchEfiVariables = true;
    };
    consoleLogLevel = 0;
    extraModprobeConfig = "options kvm_amd nested=1";
    initrd.verbose = false;
    kernel = {
      sysctl = {
        # for waydroid
        "net.ipv4.ip_forward" = 1;
        "net.ipv4.conf.all.forwarding" = 1;
        "net.ipv6.conf.all.forwarding" = 1;
      };
    };
    kernelPackages = pkgs.linuxKernel.packages.linux_zen;
    kernelParams = [
      "quiet"
      "splash"
      "boot.shell_on_fail"
      "loglevel=3"
      "rd.systemd.show_status=false"
      "rd.udev.log_level=3"
      "udev.log_priority=3"
    ];
    plymouth = {
      enable = true;
      themePackages = with pkgs; [
        (adi1090x-plymouth-themes.override {
          selected_themes = [ "hud_3" ];
        })
      ];
      theme = "hud_3";
    };
    tmp = {
      cleanOnBoot = true;
    };
  };

  environment = {
    cinnamon.excludePackages = with pkgs; [
      gnome-calendar
      gnome-terminal
      mint-artwork
      mint-cursor-themes
      mint-themes
      mint-x-icons
      mint-y-icons
      pix
      warpinator
      xplayer
    ];
    gnome.excludePackages = with pkgs; [
      epiphany
      geary
      gnome-calendar
      gnome-clocks
      gnome-connections
      gnome-contacts
      gnome-maps
      gnome-music
      gnome-tour
      gnome-weather
      simple-scan
      snapshot
      totem
      yelp
    ];
    pantheon.excludePackages = with pkgs.pantheon; [
      epiphany
      elementary-videos
      elementary-terminal
      elementary-tasks
      elementary-music
      elementary-mail
      elementary-camera
      elementary-calendar
    ];
    # pathsToLink = [ "/libexec" ];
    sessionVariables = {};
    # List packages installed in system profile.
    systemPackages = with pkgs; [
      dconf-editor
      git
      gnome-tweaks
      home-manager
      pantheon.elementary-icon-theme
      pantheon.elementary-sound-theme
      # indicator-application-gtk3
    ];
  };

  fonts = {
    packages = with pkgs; [
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-cjk-serif
      noto-fonts-color-emoji
      udev-gothic-nf
    ];
    fontDir.enable = true;
    fontconfig = {
      enable = true;
      defaultFonts = {
        sansSerif = [
          "Noto Sans CJK JP"
          "Noto Color Emoji"
        ];
        serif = [
          "Noto Serif CJK JP"
          "Noto Color Emoji"
        ];
        monospace = [
          "UDEV Gothic NFLG"
          "Noto Color Emoji"
        ];
        emoji = [ "Noto Color Emoji" ];
      };
    };
  };

  hardware = {
    pulseaudio.enable = false;
  };

  i18n = {
    defaultLocale = "ja_JP.UTF-8";
    extraLocaleSettings = {
      LC_ADDRESS = "ja_JP.UTF-8";
      LC_IDENTIFICATION = "ja_JP.UTF-8";
      LC_MEASUREMENT = "ja_JP.UTF-8";
      LC_MONETARY = "ja_JP.UTF-8";
      LC_NAME = "ja_JP.UTF-8";
      LC_NUMERIC = "ja_JP.UTF-8";
      LC_PAPER = "ja_JP.UTF-8";
      LC_TELEPHONE = "ja_JP.UTF-8";
      LC_TIME = "ja_JP.UTF-8";
    };

    inputMethod = {
      enable = true;
      type = "fcitx5";
      fcitx5= {
        waylandFrontend = true;
        addons = with pkgs; [
          fcitx5-mozc
        ];
        settings = {
          globalOptions = {
            "Hotkey" = {
              EnumerateWithTriggerKeys = true;
              AltTriggerKeys = null;
              EnumerateForwardKeys = null;
              EnumerateBackwardKeys = null;
              EnumerateSkipFirst = false;
              EnumerateGroupForwardKeys = null;
              EnumerateGroupBackwardKeys = null;
            };
            "Hotkey/TriggerKeys" = { "0" = "Super+space"; };
            "Hotkey/PrevPage" = { "0" = "Up"; };
            "Hotkey/NextPage" = { "0" = "Down"; };
            "Hotkey/PrevCandidate" = { "0" = "Shift+Tab"; };
            "Hotkey/NextCandidate" = { "0" = "Tab"; };
            "Hotkey/TogglePreedit" = { "0" = "Control+Alt+P"; };
            "Behavior" = {
              ActiveByDefault = true;
              resetStateWhenFocusIn = "No";
              ShareInputState = "All";
              PreeditEnabledByDefault = true;
              ShowInputMethodInformation = true;
              showInputMethodInformationWhenFocusIn = false;
              CompactInputMethodInformation = false;
              ShowFirstInputMethodInformation = true;
              DefaultPageSize = 5;
              OverrideXkbOption = false;
              CustomXkbOption = null;
              EnabledAddons = null;
              DisabledAddons = null;
              PreloadInputMethod = true;
              AllowInputMethodForPassword = false;
              ShowPreeditForPassword = false;
              AutoSavePeriod = 30;
            };
          };
        };
      };
    };
  };

  networking = {
    hostName = hostName;
    firewall = {
      enable = true;
    };
    nftables = {
      enable = true;
      tables = {
        filter = {
          content = ''
            chain input {
              type filter hook input priority 0
              policy drop

              ct state invalid drop
              ct state { established, related } accept

              iif lo accept
              iif != lo ip daddr 127.0.0.1/8 drop
              iif != lo ip6 daddr ::1/128 drop

              ip protocol icmp icmp type { destination-unreachable, router-advertisement, time-exceeded, parameter-problem } accept
              ip6 nexthdr icmpv6 icmpv6 type { destination-unreachable, packet-too-big, time-exceeded, parameter-problem, nd-router-advert, nd-neighbor-solicit, nd-neighbor-advert } accept

              udp dport { 67, 53 } accept
            }

            chain forward {
              type filter hook forward priority 0
              policy accept
            }
          '';
          family = "inet";
        };
      };
    };
    # wireless.enable = true;
    networkmanager.enable = true;
  };

  nix = {
    settings = {
      auto-optimise-store = true;
      experimental-features = [ "nix-command" "flakes" ];
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-then 7d";
    };
  };

  programs = {
    appimage = {
      enable = true;
      binfmt = true;
    };
    dconf.profiles = {
      gdm.databases = [{
        lockAll = true;
        settings = {
          "org/gnome/desktop/interface" = {
            cursor-theme = "elementary";
          };
        };
      }];
      user.databases = [{
        lockAll = true;
        settings = {
          "org/gnome/mutter" = {
            dynamic-workspaces = true;
          };
        };
      }];
    };
    virt-manager.enable = true;
    zsh.enable = true;
  };

  security = {
    rtkit.enable = true;
    polkit.enable = true;
  };

  services = {
    flatpak.enable = true;
    printing.enable = true;
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      jack.enable = true;
    };

    xserver = {
      enable = true;
      desktopManager = {
        cinnamon.enable = false;
        gnome.enable = true;
        pantheon = {
          enable = false;
          extraWingpanelIndicators = with pkgs; [
            wingpanel-indicator-ayatana
          ];
        };
      };
      displayManager = {
        gdm = {
          enable = true;
          autoSuspend = false;
          wayland = true; # if false, cannot launch gnome
        };
        lightdm = {
          enable = false;
          greeters = {
            pantheon.enable = false;
            slick = {
              enable = true;
              cursorTheme.package = pkgs.pantheon.elementary-icon-theme;
              cursorTheme.name = "elementary";
            };
          };
        };
      };
      excludePackages = with pkgs; [
        xterm
      ];
      xkb = {
        layout = "us";
        variant = "";
      };
    };
  };

  # systemd.user.services.indicatorapp = {
  #   description = "indicator-application-gtk3";
  #   wantedBy = [ "graphical-session.target" ];
  #   partOf = [ "graphical-session.target" ];
  #   serviceConfig = {
  #     ExecStart = "${pkgs.indicator-application-gtk3}/libexec/indicator-application/indicator-application-service";
  #   };
  # };

  time = {
    timeZone = "Asia/Tokyo";
    hardwareClockInLocalTime = true;
  };

  users.users = {
    ${userName} = {
      isNormalUser = true;
      description = userName;
      extraGroups = [ "networkmanager" "wheel" "libvirtd" ];
      hashedPassword = hashedPassword;
      createHome = true;
      shell = pkgs.zsh;
    };
  };

  xdg = {
    portal = {
      enable = true;
      extraPortals = with pkgs; [ xdg-desktop-portal-gtk ];
    };
  };

  virtualisation = {
    podman = {
      enable = true;
      dockerCompat = true;
    };
    libvirtd = {
      enable = true;
      qemu = {
        ovmf = {
          packages = [ (pkgs.OVMF.override {
            secureBoot = true;
            tpmSupport = true;
          }).fd ];
        };
        swtpm.enable = true;
      };
    };
    waydroid.enable = true;
  };

  nixpkgs.config.allowUnfree = true;

  system.stateVersion = "24.11";
}

