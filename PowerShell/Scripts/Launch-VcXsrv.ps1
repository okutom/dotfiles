$VcXsrvBin = "C:\Program Files\VcXsrv\vcxsrv.exe"
$MonitorNo1 = "Monitor 1"
$MonitorNo2 = "Monitor 2"

Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

$form = New-Object System.Windows.Forms.Form
$form.Text = 'Select a Display'
$form.Size = New-Object System.Drawing.Size(280, 120)
$form.StartPosition = 'CenterScreen'

$label = New-Object System.Windows.Forms.Label
$label.Location = New-Object System.Drawing.Point(0, 0)
$label.Size = New-Object System.Drawing.Size(280, 20)
$label.Text = 'Please select a display:'
$form.Controls.Add($label)

$comboBox = New-Object System.Windows.Forms.ComboBox
$comboBox.Location = New-Object System.Drawing.Point(10, 20)
$comboBox.Size = New-Object System.Drawing.Size(250, 20)
$comboBox.Height = 80

$okButton = New-Object System.Windows.Forms.Button
$okButton.Location = New-Object System.Drawing.Point(100, 50)
$okButton.Size = New-Object System.Drawing.Size(75, 23)
$okButton.Text = 'OK'
$okButton.DialogResult = [System.Windows.Forms.DialogResult]::OK
$form.AcceptButton = $okButton
$form.Controls.Add($okButton)

$cancelButton = New-Object System.Windows.Forms.Button
$cancelButton.Location = New-Object System.Drawing.Point(180, 50)
$cancelButton.Size = New-Object System.Drawing.Size(75, 23)
$cancelButton.Text = 'Cancel'
$cancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel
$form.CancelButton = $cancelButton
$form.Controls.Add($cancelButton)

[void] $comboBox.Items.Add($MonitorNo1)
[void] $comboBox.Items.Add($MonitorNo2)

$form.Controls.Add($comboBox)

$form.Topmost = $true

$result = $form.ShowDialog()

if ($result -eq [System.Windows.Forms.DialogResult]::OK) {
    $x = $comboBox.SelectedItem
    if ($x -eq $MonitorNo1) {
        $ScreenNo = "2"
    } elseif ($x -eq $MonitorNo2) {
        $ScreenNo = "1"
    }
    
    $VcXsrvOpt = "-screen 0 @" + $ScreenNo + " -ac -clipboard -keyhook -nowinkill -wgl -rootless"
    [System.Diagnostics.Process]::Start($VcXsrvBin, $VcXsrvOpt)
}