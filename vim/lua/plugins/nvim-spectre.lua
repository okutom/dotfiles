return {
    'nvim-pack/nvim-spectre',
    lazy = true,
    event = 'VeryLazy',
    dependencies = {
        'nvim-lua/plenary.nvim',
        'BurntSushi/ripgrep',
        'nvim-tree/nvim-web-devicons',
    },
    opts = {},
    keys = {
        {
            mode = { 'n' },
            '<leader>ss',
            function() require('spectre').toggle() end,
            desc = 'Toggle Spectre',
        },
        {
            mode = { 'n' },
            '<leader>sw',
            function()
                require('spectre').open_visual({ select_word = true })
            end,
            desc = 'Search current word',
        },
        {
            mode = { 'v' },
            '<leader>sw',
            function()
                require('spectre').open_visual()
            end,
            desc = 'Search current word',
        },
        {
            mode = { 'n' },
            '<leader>sp',
            function()
                require('spectre').open_file_search({ select_word = true })
            end,
            desc = 'Search on current file',
        },
    },
}
