return {
    'danymat/neogen',
    dependencies = {
        'nvim-treesitter/nvim-treesitter',
    },
    lazy = true,
    event = 'BufEnter',
    opts = {},
}
