return {
    'kevinhwang91/nvim-ufo',
    lazy = true,
    event = 'VeryLazy',
    dependencies = {
        'kevinhwang91/promise-async',
        'nvim-treesitter/nvim-treesitter'
    },
    init = function()
        vim.o.foldcolumn = '1'
        vim.o.foldlevel = 99
        vim.o.foldlevelstart = 99
        vim.o.foldenable = true
    end,
    config = function()
        require('ufo').setup({
            provider_selector = function(bufnr, filetype, buftype)
                return { 'treesitter', 'indent' }
            end
        })
    end,
    keys = {
        {
            mode = { 'n' },
            'zR',
            function() require('ufo').openAllFolds() end,
            desc = 'Open all folds'
        },
        {
            mode = { 'n' },
            'zM',
            function() require('ufo').closeAllFolds() end,
            desc = 'Close all folds'
        },
    },
}
