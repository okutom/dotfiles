""" vim
" ←↓↑→
noremap k <Left>
noremap t <Down>
noremap n <Up>
noremap s <Right>
" カーソル下の文字を削除し挿入モードにする(もともとのs)
noremap S s

" search
" [n]ext -> [j]ump
noremap j n
noremap J N

" join
noremap gj J

" go ahead (beginning of sentence)
noremap <C-i> ^
cnoremap <C-i> <Home>
inoremap <C-i> <Esc>I
" go ahead (beginning of line)
noremap <C-S-i> 0
cnoremap <C-S-i> <Home>
inoremap <C-S-i> <Esc>0i

" go end
noremap <C-a> $
cnoremap <C-a> <End>
inoremap <C-a> <Esc>A

" ペースト
inoremap <C-p> <Esc>pa
inoremap <C-S-p> <Esc>Pa

" スクロール
noremap <C-u> <C-b>
noremap <C-e> <C-f>
noremap <C-S-u> <C-u>
noremap <C-S-e> <C-d>

" ウィンドウ操作
noremap <C-w>k <C-w>h
inoremap <C-w>k <C-w>h
noremap <C-w>t <C-w>j
inoremap <C-w>t <C-w>j
noremap <C-w>n <C-w>k
inoremap <C-w>n <C-w>k
noremap <C-w>s <C-w>l
inoremap <C-w>s <C-w>l

noremap <C-w>K <C-w>H
inoremap <C-w>K <C-w>H
noremap <C-w>T <C-w>J
inoremap <C-w>T <C-w>J
noremap <C-w>N <C-w>K
inoremap <C-w>N <C-w>K
noremap <C-w>S <C-w>L
inoremap <C-w>S <C-w>L

" 右にインデント
nnoremap <C-.> >>
inoremap <C-.> <C-t>
" 左にインデント
nnoremap <C-,> <<
inoremap <C-,> <C-d>

" 1行下に空行を入れる
nnoremap <C-CR> mzo<Esc>`z
inoremap <C-CR> <End><CR>
" 1行上に空行を入れる
nnoremap <C-S-CR> mzO<Esc>`z
inoremap <C-S-CR> <Home><CR><Up>

" ハイライトを消去
nnoremap <silent> <C-l> :<C-u>nohlsearch<CR><C-l>

""" emacs hybrid
" cancel
noremap <C-g> <Esc>
inoremap <C-g> <Esc>a
cnoremap <C-g> <Esc>
" delete character
inoremap <C-d> <del>
cnoremap <C-d> <del>
" ←↓↑→
nnoremap <C-k> <left>
vnoremap <C-k> <left>
inoremap <C-k> <left>
cnoremap <C-k> <left>
nnoremap <C-t> <down>
vnoremap <C-t> <down>
inoremap <C-t> <down>
cnoremap <C-t> <down>
nnoremap <C-n> <up>
vnoremap <C-n> <up>
inoremap <C-n> <up>
cnoremap <C-n> <up>
nnoremap <C-s> <right>
vnoremap <C-s> <right>
inoremap <C-s> <right>
cnoremap <C-s> <right>
