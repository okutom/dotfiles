return {
    'arnaupv/nvim-devcontainer-cli',
    enabled = false,
    lazy = true,
    event = 'VeryLazy',
    opts = {
        nvim_dotfiles_repo = 'https://gitlab.com/okutom/dotfiles.git',
        nvim_dotfiles_install_command = 'cd ~/dotfiles && ./install.sah',
    },
}
