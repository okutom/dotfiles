#!/usr/bin/env zsh

if type ssh-agent > /dev/null 2>&1 && [ -e ~/.ssh ]; then
  if [ -z "${SSH_AUTH_SOCK}" ]; then
    # Check for a currently running instance of the agent
    RUNNING_AGENT="`ps -ax | grep 'ssh-agent -s' | grep -v grep | wc -l | tr -d '[:space:]'`"
    if [ "${RUNNING_AGENT}" = "0" ]; then
      # Launch a new instance of the agent
      ssh-agent -s | awk 'NR != 3 {print $0;}' &> ~/.ssh/ssh-agent
    fi
    eval `cat ~/.ssh/ssh-agent`
  fi
fi
