return {
    'nvim-lualine/lualine.nvim',
    lazy = true,
    event = 'VeryLazy',
    dependencies = {
        'nvim-tree/nvim-web-devicons',
        'WhoIsSethDaniel/lualine-lsp-progress.nvim',
    },
    opts = {
        options = {
            theme = 'monokai-pro',
            disabled_filetypes = { 'neo-tree' },
        },
        sections = {
            lualine_a = {'mode'},
            lualine_b = {'branch', 'diff', 'diagnostics'},
            lualine_c = { 'searchcount', 'selectioncount', 'filename', 'lsp_progress' },
            lualine_x = {'encoding', 'fileformat', 'filetype'},
            lualine_y = {'progress'},
            lualine_z = {'location'}
        },
    },
}
