return {
    -- Patch for lazy.nvim's bug in GUI(neovim-qt)
    'equalsraf/neovim-gui-shim',
    lazy = true,
    event = 'VeryLazy'
}
