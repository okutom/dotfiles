#!/usr/bin/env bash

if ! type xdg-user-dir &>/dev/null; then
    echo xdg-user-dir does not installed.
    echo please install it first.
    exit
fi

name=(
    'DESKTOP'
    'DOWNLOAD'
    'TEMPLATES'
    'PUBLICSHARE'
    'DOCUMENTS'
    'MUSIC'
    'PICTURES'
    'VIDEOS'
)

# bash array indexes start at "0"
guessdir() {
    if [[ "${1}" == "${name[0]}" ]]; then
        echo "${HOME}/Desktop"
    elif [[ "${1}" == "${name[1]}" ]]; then
        echo "${HOME}/Downloads"
    elif [[ "${1}" == "${name[2]}" ]]; then
        echo "${HOME}/Templates"
    elif [[ "${1}" == "${name[3]}" ]]; then
        echo "${HOME}/Public"
    elif [[ "${1}" == "${name[4]}" ]]; then
        echo "${HOME}/Documents"
    elif [[ "${1}" == "${name[5]}" ]]; then
        echo "${HOME}/Music"
    elif [[ "${1}" == "${name[6]}" ]]; then
        echo "${HOME}/Pictures"
    elif [[ "${1}" == "${name[7]}" ]]; then
        echo "${HOME}/Videos"
    fi
}

for n in "${name[@]}"
do
    tmp_path=$(xdg-user-dir "${n}")
    echo "${n}" is set to "${tmp_path}".
    if [[ "${tmp_path}" == "${HOME}" ]]; then
        gd=$(guessdir "${n}")
        echo now set "${n}" to "${gd}"
        if [[ ! -e "${gd}" ]]; then
            echo "${gd}" does not exist.
            echo make it!
            mkdir -p "${gd}"
        fi
        xdg-user-dirs-update --set "${n}" "${gd}"
    else
        if [[ "${tmp_path}" == "${HOME}/Desktop" ]]; then
            if [[ ! -e "${HOME}/Desktop" ]]; then
                echo but, the directory does not exist.
                echo make it!
                mkdir -p "${HOME}/Destkop"
            fi
        else
            echo skip
        fi
    fi
done
