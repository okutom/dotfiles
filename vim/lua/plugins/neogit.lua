return {
    'NeogitOrg/neogit',
    dependencies = {
        'nvim-lua/plenary.nvim',
        'sindrets/diffview.nvim',
        'nvim-telescope/telescope.nvim',
    },
    lazy = true,
    event = 'VeryLazy',
    config = true,
    opts = {
        mappings = {
            popup = {
                ["t"] = false,
                ["T"] = "TagPopup",
            },
            status = {
                ["<c-s>"] = false,
                ["<A-s>"] = "StageAll",
                ["<c-t>"] = false,
                ["<A-t>"] = "TabOpen",
            },
        },
    },
    keys = {
        {
            mode = { 'n' },
            '<leader>go',
            function()
                require('neogit').open()
            end,
            desc = 'open',
        },
        {
            mode = { 'n' },
            '<leader>gc',
            function()
                require('neogit').open({ 'commit' })
            end,
            desc = 'open commit popup',
        },
        {
            mode = { 'n' },
            '<leader>gO',
            function()
                require('neogit').open({ kind = 'auto' })
            end,
            desc = 'open with split kind',
        },
    },
}
