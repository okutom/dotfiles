vim.o.guifont = 'UDEV Gothic NFLG:h14'
vim.opt.linespace = 0
vim.g.neovide_scale_factor = 1.0

vim.g.neovide_padding_top = 0
vim.g.neovide_padding_bottom = 0
vim.g.neovide_padding_right = 0
vim.g.neovide_padding_left = 0

vim.g.neovide_scroll_animation_length = 0.1
vim.g.neovide_scroll_animation_far_lines = 1

vim.g.neovide_hide_mouse_when_typing = true

vim.g.neovide_underline_stroke_scale = 1.0

vim.g.neovide_unlink_border_highlights = true

vim.g.neovide_refresh_rate = 120
vim.g.neovide_refresh_rate_idle = 5
-- vim.g.neovide_no_idle = false

-- vim.g.neovide_confirm_quit = true

-- vim.g.neovide_fullscreen = true
vim.g.neovide_remember_window_size = false

local function set_ime(args)
    if args.event:match('Enter$') then
        vim.g.neovide_input_ime = true
    else
        vim.g.neovide_input_ime = false
    end
end
local ime_input = vim.api.nvim_create_augroup('ime_input', { clear = true })

vim.api.nvim_create_autocmd({ 'InsertEnter', 'InsertLeave' }, {
    group = ime_input,
    pattern = '*',
    callback = set_ime,
})

vim.api.nvim_create_autocmd({ 'CmdlineEnter', 'CmdlineLeave' }, {
    group = ime_input,
    pattern = '[/\\?]',
    callback = set_ime,
})

vim.g.neovide_cursor_animation_length = 0
vim.g.neovide_cursor_trail_sizu = 0
vim.g.neovide_cursor_antialiasing = true
vim.g.neovide_cursor_animate_in_insert_mode = false
vim.g.neovide_cursor_animate_command_line = false
vim.g.neovide_cursor_unfocused_outline_width = 0.125
vim.g.neovide_cursor_smooth_blink = false
