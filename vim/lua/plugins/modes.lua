return {
    'mvllow/modes.nvim',
    version = 'v0.2.1',
    lazy = true,
    event = 'VeryLazy',
    opts = {
        colors = {
            copy = '#fd971f',
            delete = '#f92672',
            insert = '#a6e22e',
            visual = '#ae81ff',
        },
        line_opecity = 0.15,
        set_cursor = true,
        set_cursorline = true,
        set_number = true,
        ignore_filetypes = { 'neo-tree', 'TelescopePrompt' },
    },
}
