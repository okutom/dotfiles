-- https://github.com/streetturtle/awesome-wm-widgets/tree/master/volume-widget
-- https://github.com/streetturtle/awesome-wm-widgets/blob/master/LICENSE
local awful = require("awful")
local beautiful = require('beautiful')
local wibox = require("wibox")

local audio = {}

local function get_widget()
    local icon_muted = beautiful.volume_muted
    local icon_low = beautiful.volume_low
    local icon_medium = beautiful.volume_medium
    local icon_high = beautiful.volume_high
    return wibox.widget {
        {
            {
                id = "icon",
                resize = true,
                widget = wibox.widget.imagebox,
            },
            valign = 'center',
            layout = wibox.container.place
        },
        {
            id = 'txt',
            font = beautiful.font,
            widget = wibox.widget.textbox
        },
        layout = wibox.layout.fixed.horizontal,
        set_volume_level = function(self, new_value)
            self:get_children_by_id('txt')[1]:set_text(new_value .. "% ")
            local volume_icon
            if self.is_muted then
                volume_icon = icon_muted
            else
                local new_value_num = tonumber(new_value)
                if (0 <= new_value_num and new_value_num < 33) then
                    volume_icon = icon_low
                elseif (new_value_num < 66) then
                    volume_icon = icon_medium
                else
                    volume_icon = icon_high
                end
            end
            self:get_children_by_id('icon')[1]:set_image(volume_icon)
        end,
        mute = function(self)
            self.is_muted = true
            self:get_children_by_id('icon')[1]:set_image(icon_muted)
        end,
        un_mute = function(self)
            self.is_muted = false
        end
    }
end

local function worker()
    local refresh_rate = 1
    local default_step = 1
    audio.widget = get_widget()

    local function update_graphic(widget, stdout)
        local mute = string.match(stdout, "%[(o%D%D?)%]")
        if mute == "off" then
            widget:mute()
        elseif mute == "on" then
            widget:un_mute()
        end
        local volume_level = string.match(stdout, "(%d?%d?%d)%%")
        volume_level = string.format("%d", volume_level)
        widget:set_volume_level(volume_level)
    end

    function audio:inc(s)
        local step = s or default_step
        awful.spawn.easy_async(
            "sh -c 'amixer -D pulse sset Master " .. step .. "%+'",
            function(stdout) update_graphic(audio.widget, stdout) end
        )
    end

    function audio:dec(s)
        local step = s or default_step
        awful.spawn.easy_async(
            "sh -c 'amixer -D pulse sset Master " .. step .. "%-'",
            function(stdout) update_graphic(audio.widget, stdout) end
        )
    end

    function audio:toggle()
        awful.spawn.easy_async(
            "sh -c 'amixer -D pulse sset Master toggle'",
            function(stdout) update_graphic(audio.widget, stdout) end
        )
    end

    function audio:mixer()
        awful.spawn.raise_or_spawn(
            'pavucontrol',
            {
                floating = true,
                width = 500,
                height = 400,
                placement = awful.placement.top_right
            }
        )
    end

    audio.widget:buttons(
        awful.util.table.join(
            awful.button({ }, 4, function() audio:inc() end),
            awful.button({ }, 5, function() audio:dec() end),
            awful.button({ }, 3, function() audio:mixer() end),
            awful.button({ }, 1, function() audio:toggle() end)
        )
    )
    awful.widget.watch("amixer -D pulse sget Master", refresh_rate, update_graphic, audio.widget)

    return audio.widget
end

audio.applet = setmetatable(audio, { __call = function(_, ...) return worker() end })

return audio
