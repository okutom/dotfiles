return {
    'pocco81/auto-save.nvim',
    lazy = true,
    event = 'VeryLazy',
    opts = {
        enabled = true,
        execution_message = {
            message = '',
        },
        trigger_events = {
            'InsertLeave',
            'TextChanged',
        },
        debounce_delay = 5000,
    },
}
