return {
    'williamboman/mason-lspconfig.nvim',
    lazy = true,
    event = 'VeryLazy',
    dependencies = {
        'neovim/nvim-lspconfig',
        {
            'williamboman/mason.nvim',
            build = ':MasonUpdate',
            opts = {},
        },
        {
            'jay-babu/mason-null-ls.nvim',
            dependencies = {
                {
                    'nvimtools/none-ls.nvim',
                    main = 'null-ls',
                },
            },
            event = { 'BufReadPre', 'BufNewFile' },
            opts = {
                ensure_installed = {},
            },
        },
        {
            'jay-babu/mason-nvim-dap.nvim',
            dependencies = {
                'mfussenegger/nvim-dap',
            },
            opts = {
                ensure_installed = {},
            },
        },
        'hrsh7th/cmp-nvim-lsp',
    },
    opts = {
        ensure_installed = {
            'bashls',
            'clangd',
            'cmake',
            'cssls',
            'denols',
            'dockerls',
            'docker_compose_language_service',
            'eslint',
            'gopls',
            'gradle_ls',
            'groovyls',
            'html',
            'jsonls',
            'java_language_server',
            'ts_ls',
            'lua_ls',
            'marksman',
            'powershell_es',
            'pylsp',
            'rust_analyzer',
            'sqlls',
            'taplo',
            'tailwindcss',
            'lemminx',
            'yamlls',
        },
        automatic_installation = true,
    },
    config = function()
        local lspconfig = require('lspconfig')
        local capabilities = require('cmp_nvim_lsp').default_capabilities()
        lspconfig.bashls.setup({
            capabilities = capabilities,
        })
        lspconfig.clangd.setup({
            capabilities = capabilities,
        })
        lspconfig.cmake.setup({
            capabilities = capabilities,
        })
        lspconfig.cssls.setup({
            capabilities = capabilities,
        })
        lspconfig.dockerls.setup({
            capabilities = capabilities,
        })
        lspconfig.docker_compose_language_service.setup({
            capabilities = capabilities,
        })
        lspconfig.eslint.setup({
            capabilities = capabilities,
        })
        lspconfig.gopls.setup({
            capabilities = capabilities,
        })
        lspconfig.gradle_ls.setup({
            capabilities = capabilities,
        })
        lspconfig.groovyls.setup({
            capabilities = capabilities,
        })
        lspconfig.html.setup({
            capabilities = capabilities,
        })
        lspconfig.java_language_server.setup({
            capabilities = capabilities,
        })
        lspconfig.jsonls.setup({
            capabilities = capabilities,
        })
        lspconfig.ts_ls.setup({
            capabilities = capabilities,
        })
        lspconfig.lua_ls.setup({
            capabilities = capabilities,
        })
        lspconfig.marksman.setup({
            capabilities = capabilities,
        })
        lspconfig.powershell_es.setup({
            capabilities = capabilities,
        })
        lspconfig.pylsp.setup({
            capabilities = capabilities,
        })
        lspconfig.rust_analyzer.setup({
            on_attach = function(client, bufnr)
                vim.lsp.inlay_hint.enable(bufnr)
            end,
            settings = {
                ['rust_analyzer'] = {
                    imports = {
                        granularity = {
                            group = 'module',
                        },
                        prefix = 'self',
                    },
                    cargo = {
                        buildScripts = {
                            enable = true,
                        },
                    },
                    procMacro = {
                        enable = true,
                    },
                },
            },
            capabilities = capabilities,
        })
        lspconfig.sqlls.setup({
            capabilities = capabilities,
        })
        lspconfig.taplo.setup({
            capabilities = capabilities,
        })
        lspconfig.tailwindcss.setup({
            capabilities = capabilities,
        })
        lspconfig.lemminx.setup({
            capabilities = capabilities,
        })
        lspconfig.yamlls.setup({
            capabilities = capabilities,
        })
    end,
}
