return {
    'winston0410/range-highlight.nvim',
    lazy = true,
    event = 'VeryLazy',
    dependencies = {
        'winston0410/cmd-parser.nvim',
    },
    opts = {},
}
