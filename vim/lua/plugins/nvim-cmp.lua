return {
    'hrsh7th/nvim-cmp',
    lazy = true,
    event = 'InsertEnter',
    dependencies = {
        'hrsh7th/cmp-path',
        'hrsh7th/cmp-buffer',
        'ray-x/cmp-treesitter',
        {
            'f3fora/cmp-spell',
            init = function()
                vim.o.spell = false
                vim.o.spelllang = 'en_us'
            end,
        },
        'bydlw98/cmp-env',
        'hrsh7th/cmp-cmdline',
        'hrsh7th/cmp-nvim-lsp',
        'hrsh7th/cmp-nvim-lsp-signature-help',
        'hrsh7th/cmp-nvim-lsp-document-symbol',
        {
            'saadparwaiz1/cmp_luasnip',
            dependencies = { 'L3MON4D3/LuaSnip' },
        },
        'onsails/lspkind.nvim',
        'ray-x/lsp_signature.nvim',
    },
    config = function()
        local cmp = require('cmp')
        local types = require('cmp.types')
        local str = require('cmp.utils.str')
        local lspkind = require('lspkind')

        cmp.setup({
            window = {
                completion = cmp.config.window.bordered(),
                documentation = cmp.config.window.bordered(),
            },
            sources = {
                { name = 'luasnip' },
                { name = 'nvim_lsp' },
                { name = 'nvim_lsp_signature_help' },
                { name = 'buffer' },
                { name = 'path' },
                { name = 'env' },
                { name = 'treesitter' },
                {
                    name = 'spell',
                    option = {
                        keep_all_entries = false,
                        enable_in_context = function()
                            return true
                        end,
                    },
                },
            },
            snippet = {
                expand = function(args)
                    require('luasnip').lsp_expand(args.body)
                end
            },
            formatting = {
                format = lspkind.cmp_format({
                    mode = 'symbol',
                    maxwidth = 50,
                    ellipsis_char = '...',
                    before = function(entry, vim_item)
                        local word = entry:get_insert_text()
                        if entry.completion_item.insertTextFormat == types.lsp.InsertTextFormat.Snippet then
                            word = vim.lsp.util.parse_snippet(word)
                        end
                        word = str.oneline(word)

                        if entry.completion_item.insertTextFormat == types.lsp.InsertTextFormat.Snippet and string.sub(vim_item.abbr, -1, -1) == '~' then
                            word = word .. '~'
                        end
                        vim_item.abbr = word

                        return vim_item
                    end
                })
            },
            mapping = cmp.mapping.preset.insert({
                ['<C-g>'] =  cmp.mapping.abort(),
                ['<CR>'] = cmp.mapping.confirm({ select = true }),
                ['<Tab>'] = cmp.mapping.confirm({ select = true }),
                ['<C-Space>'] = cmp.mapping.complete(),
                ['<C-t>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
                ['<C-n>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
                ['<C-p>'] = function(fallback) fallback() end,
            }),
        })

        cmp.setup.cmdline({ '/', '?' }, {
            window = {
                completion = cmp.config.window.bordered(),
                documentation = cmp.config.window.bordered(),
            },
            sources = cmp.config.sources({
                { name = 'nvim_lsp_document_symbol' }
            }, {
                { name = 'buffer' }
            }),
            mapping = cmp.mapping.preset.cmdline(),
        })

        cmp.setup.cmdline(':', {
            window = {
                completion = cmp.config.window.bordered(),
                documentation = cmp.config.window.bordered(),
            },
            sources = cmp.config.sources({
                { name = 'path' }
            }, {
                { name = 'cmdline' }
            }),
            mapping = cmp.mapping.preset.cmdline(),
        })

        local cfg = {
            handler_opts = { border = 'shadow' },
        }
        require('lsp_signature').setup(cfg)
    end,
}
