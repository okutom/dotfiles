return {
    'loctvl842/monokai-pro.nvim',
    lazy = true,
    event = 'VimEnter *',
    config = function()
        require('monokai-pro').setup({
            background_clear = {
                'float_win',
                'toggleterm',
                'telescope',
                'which-key',
                'renamer',
                'notify',
                'nvim-tree',
                'neo-tree',
                'bufferline',
                'indent-blankline',
            },
        })
        vim.cmd([[colorscheme monokai-pro-classic]])
    end,
}
