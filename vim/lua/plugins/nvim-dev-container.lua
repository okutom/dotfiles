return {
    'esensar/nvim-dev-container',
    enabled = false,
    lazy = true,
    dependencies = 'nvim-treesitter/nvim-treesitter',
    event = 'VeryLazy',
    opts = {
        attach_mounts = {
            always = true,
            neovim_config = {
                enabled = true,
                options = { 'readonly' },
            },
            neovim_data = {
                enabled = false,
                options = {},
            },
            neovim_state = {
                enabled = false,
                options = {},
            },
        },
        container_runtime = 'podman',
        backup_runtime = nil,
        compose_command = 'devcontainer-cli',
        backup_compose_command = nil,
    },
}
