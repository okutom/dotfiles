vim.g.mapleader = " "

vim.o.clipboard = 'unnamedplus'
vim.o.hlsearch = true
vim.o.ignorecase = true
vim.o.incsearch = true
vim.o.timeout = false
vim.o.number = true
vim.o.relativenumber = true
vim.o.showmode = true
vim.o.smartcase = true
vim.o.visualbell = true

-- indent
vim.o.tabstop = 4
vim.o.shiftwidth = 4
vim.o.softtabstop = 4
vim.o.expandtab = true
vim.o.autoindent = true
vim.o.smartindent = true

-- load files
if vim.g.neovide then
    require('env.neovide')
end
if vim.g.vscode then
    require('env.vscode')
end
if vim.fn.has('wsl') == 1 then
    require('env.wsl')
end
require('install-lazy-nvim')
require('keymap.global')
require('keymap.astarte')
