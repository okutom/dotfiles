-- ←↓↑→
vim.keymap.set({ 'n', 'v' }, 'k', 'h')
vim.keymap.set({ 'n', 'v' }, 't', 'j')
vim.keymap.set({ 'n', 'v' }, 'n', 'k')
vim.keymap.set({ 'n', 'v' }, 's', 'l')
vim.keymap.set({ 'n', 'v', 'c', 'i' }, '<C-k>', '<Left>')
vim.keymap.set({ 'n', 'v', 'c', 'i' }, '<C-t>', '<Down>')
vim.keymap.set({ 'n', 'v', 'c', 'i' }, '<C-n>', '<Up>')
vim.keymap.set({ 'n', 'v', 'c', 'i' }, '<C-s>', '<Right>')

-- delete & insert (swap s & h)
vim.keymap.set({ 'n', 'v' }, 'h', 's')
vim.keymap.set({ 'n', 'v' }, 'H', 'S')

-- search
-- [n]ext → [j]ump
vim.keymap.set({ 'n' }, 'j', 'n', { noremap = true })
vim.keymap.set({ 'n' }, 'J', 'N', { noremap = true })

-- join
vim.keymap.set({ 'n' }, 'gj', 'J', { noremap = true })

-- window
vim.keymap.set({ 'n', 'i' }, '<C-w>k', '<C-w>h', { noremap = true })
vim.keymap.set({ 'n', 'i' }, '<C-w>t', '<C-w>j', { noremap = true })
vim.keymap.set({ 'n', 'i' }, '<C-w>n', '<C-w>k', { noremap = true })
vim.keymap.set({ 'n', 'i' }, '<C-w>s', '<C-w>l', { noremap = true })

vim.keymap.set({ 'n', 'i' }, '<C-w>K', '<C-w>H', { noremap = true })
vim.keymap.set({ 'n', 'i' }, '<C-w>T', '<C-w>J', { noremap = true })
vim.keymap.set({ 'n', 'i' }, '<C-w>N', '<C-w>K', { noremap = true })
vim.keymap.set({ 'n', 'i' }, '<C-w>S', '<C-w>L', { noremap = true })
