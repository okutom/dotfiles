return {
    'michaelb/sniprun',
    branch = 'master',
    lazy = true,
    event = 'VeryLazy',
    build = 'sh ./install.sh',
    opts = {},
}
