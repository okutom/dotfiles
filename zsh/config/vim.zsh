#!/usr/bin/env zsh

if type "bob" &>/dev/null; then
    export PATH=${HOME}/.local/share/bob/nvim-bin:${PATH}
fi
