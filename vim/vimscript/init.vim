if exists('g:vscode')
    lua << EOF
    require('astarte.lua')
    EOF
else
    set noerrorbells

    let mapleader = "\<Space>"

    " 行を上に移動
    nnoremap <A-S-n> :m .-2<CR>==
    inoremap <A-S-n> <Esc>:m .-2<CR>==gi
    vnoremap <A-S-n> :m '<-2<CR>gv=gv
    " 行を下に移動
    nnoremap <A-S-t> :m .+1<CR>==
    inoremap <A-S-t> <Esc>:m .+1<CR>==gi
    vnoremap <A-S-t> :m '>+1<CR>gv=gv

    """ load mappings
    source ~/.dotfiles/vim/vimscript/astarte.vim
    source ~/.dotfiles/vim/vimscript/settings.vim
endif
