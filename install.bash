#!/usr/bin/env bash

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

if [ -d /mnt/c ]; then
  umask 0022
fi

# init_external() {
# }

install() {
  for i in "${abs_script_dir}"/*; do
    if [ -d "${i}" ] && [ ! "${i}" = "${abs_script_dir}/.git" ] && [ ! "${i}" = "${abs_script_dir}/ext" ]; then
      "${i}/install.bash"
    elif [ "${i}" = "${abs_script_dir}/ext" ]; then
      for j in "${i}"/*; do
        "${j}/install.bash"
      done
    fi
  done
}

# init_external
install

unset abs_script_dir
unset script_dir

exit 0
