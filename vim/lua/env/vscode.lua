vim.keymap.set({ 'n', 'v' }, 'gc', '<Plug>VSCodeCommentary')
vim.keymap.set({ 'n' }, 'gcc', '<Plug>VSCodeCommentaryLine')