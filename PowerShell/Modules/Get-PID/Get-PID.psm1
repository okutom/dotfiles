Param(
    [String]$procName
)

function Get-PID {
    Param($name)
    $ErrorActionPreference = "silentlycontinue"

    $TargetPid = Get-Process -Name $name | ForEach-Object {$_.Id}
    If ($? -eq $True) {
        return $TargetPid
    } Else {
        return -1
    }
}
