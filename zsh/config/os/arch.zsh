#!/usr/bin/env zsh

# Load command-not-found on ArchLinux based distributions
if [[ -s /usr/share/doc/pkgfile/command-not-found.zsh ]]; then
  source /usr/share/doc/pkgfile/command-not-found.zsh
fi

if type "yay" &>/dev/null; then
  alias update='yay -Syu'
else
  alias update='sudo pacman -Syu'
fi
alias autoremove='pacman -Qtdq | sudo pacman -Rsn -'
alias listfile='pacman -Fl'
