#!/usr/bin/env zsh

## Load Settings ##
autoload -Uz colors && colors
autoload -Uz compinit && compinit

for zfile in "${ZDOTDIR}"/config/??*; do
  source "${zfile}"
done

## Editor ##
if type "nvim" &>/dev/null; then
  export EDITOR='nvim'
  export VISUAL='nvim'
elif type "vim" &>/dev/null; then
  export EDITOR='vim'
  export VISUAL='vim'
elif type "vi" &>/dev/null; then
  export EDITOR='vi'
  export VISUAL='vi'
elif type "emacs" &>/dev/null; then
  export EDITOR='emacs'
  export VISUAL='emacs'
fi


## Temporary Files ##
if [[ ! -d "${TMPDIR}" ]]; then
  export TMPDIR="/tmp/${LOGNAME}"
  mkdir -p -m 700 "${TMPDIR}"
fi
TMPPREFIX="$(mktemp -d)/zsh"


## History ##
export HISTFILE="${ZDOTDIR}/.zhistory"
export HISTSIZE=10000
export SAVEHIST=10000


## Completion ##
# Compile the completion dump to increase startup speed.
local zcompdump="${ZDOTDIR}/.zcompdump"
if [[ -s "${zcompdump}" && (! -s "${zcompdump}.zwc" || "${zcompdump}" -nt "${zcompdump}.zwc") ]]; then
  zcompile "${zcompdump}"
fi

## Theme ##
# Load starship theme, if exists
if type starship &>/dev/null; then
  if [[ -e ${HOME}/.config/starship/starship-nf.toml ]]; then
    export STARSHIP_CONFIG=${HOME}/.config/starship/starship-nf.toml
  fi
  eval "$(starship init zsh)"
fi
