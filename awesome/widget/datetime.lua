-- https://awesomewm.org/doc/api/classes/wibox.widget.calendar.html
local awful = require("awful")
local beautiful = require("beautiful")
local gears = require("gears")
local wibox = require("wibox")

-- load my theme
beautiful.init(gears.filesystem.get_configuration_dir() .. "/theme/theme.lua")

local function rect(size)
    return function(cr, width, height) gears.shape.rounded_rect(cr, width, height, size) end
end

local styles = {}
-- カレンダーの大枠
styles.month = {
    padding = 5,
    bg_color = beautiful.color['black'],
    shape = rect(0)
}
-- 今日以外
styles.normal = { shape = rect(0) }
-- 今日
styles.focus = {
    fg_color = beautiful.color['orange'],
    markup = function(t) return '<b>' .. t .. '</b>' end,
    shape = rect(0),
    border_color = beautiful.color['orange'],
    border_width = 2
}
-- 月年
styles.header = {
    fg_color = beautiful.color['white'],
    bg_color = beautiful.color['black'],
    markup = function(t) return '<b>&lt;&lt; ' .. t .. ' &gt;&gt;</b>' end,
    shape = rect(0)
}
-- 曜日
styles.weekday = {
    fg_color = beautiful.color['black'],
    bg_color = beautiful.color['yellow'],
    markup = function(t) return '<b>' .. t .. '</b>' end,
    shape = rect(0)
}

local function decorate_cell(widget, flag, date)
    if flag == 'monthheader' and not styles.monthheader then
        flag = 'header'
    end
    local props = styles[flag] or {}
    if props.markup and widget.get_text and widget.set_markup then
        widget:set_markup(props.markup(widget:get_text()))
    end
    local d = { year = date.year, month = (date.month or 1), day = (date.day or 1) }
    local weekday = tonumber(os.date('%w', os.time(d)))
    local sun_sat_fg = (weekday == 0 or weekday == 6) and beautiful.color['black'] or beautiful.color['white']
    local sun_sat_bg = weekday == 0 and beautiful.color['magenta'] or weekday == 6 and beautiful.color['cyan']
    local ret = wibox.widget {
        {
            widget,
            margins = props.padding or 2,
            widget = wibox.container.margin
        },
        shape = props.shape,
        shape_border_color = props.border_color,
        shape_border_width = props.border_width or 0,
        fg = props.fg_color or sun_sat_fg,
        bg = props.bg_color or sun_sat_bg,
        widget = wibox.container.background
    }
    return ret
end

local datetime = {}
datetime.date_time = wibox.widget.textclock("%Y/%m/%d(%a) %H:%M:%S", 1, "Asia/Tokyo")
datetime.calendar = wibox.widget {
    date = os.date('*t'),
    font = beautiful.font_name .. '24',
    start_sunday = true,
    long_weekdays = true,
    fn_embed = decorate_cell,
    widget = wibox.widget.calendar.month
}
datetime.widget = wibox.widget {
    {
        datetime.date_time,
        margins = 4,
        layout = wibox.container.margin,
    },
    shape = rect(0),
    widget = wibox.container.background,
}
datetime.popup = awful.popup {
    ontop = true,
    visible = false,
    shape = rect(0),
    border_width = beautiful.border_width,
    border_color = beautiful.border_focus,
    widget = {}
}

local function worker()
    local box = {
        layout = wibox.layout.fixed.vertical,
        datetime.calendar
    }
    datetime.popup:setup(box)
    datetime.widget:buttons(
        awful.util.table.join(
            awful.button({}, 1, function()
                if datetime.popup.visible then
                    datetime.popup.visible = not datetime.popup.visible
                    datetime.widget:set_bg(beautiful.bg_normal)
                else
                    datetime.popup:move_next_to(mouse.current_widget_geometry)
                    datetime.widget:set_bg(beautiful.bg_focus)
                end
            end)
        )
    )

    return datetime.widget
end

datetime.applet = setmetatable(datetime.widget, { __call = function(_, ...) return worker() end })

return datetime
