#!/usr/bin/env zsh

# C-A
bindkey "^A" end-of-line
# C-H
bindkey "^H" backward-delete-char
# C-K
bindkey "^K" backward-char
# C-N
bindkey "^N" up-line-or-history
# C-P
bindkey "^P" yank
# C-S
bindkey "^S" forward-char
# C-T
bindkey "^T" down-line-or-history
# C-X
bindkey "^X" kill-line

# M-A
bindkey "^[a" end-of-line
bindkey "^[A" end-of-line
# M-D
bindkey "^[d" delete-word
bindkey "^[D" delete-word
# M-H
bindkey "^[h" backward-delete-word
bindkey "^[H" backward-delete-word
# M-I
bindkey "^[i" beginning-of-line
bindkey "^[I" beginning-of-line
# M-K
bindkey "^[k" backward-word
bindkey "^[K" backward-word
# M-S
bindkey "^[s" forward-word
bindkey "^[S" forward-word
# M-]
bindkey "^[]" history-beginning-search-forward
# M-[
bindkey "^[[" history-beginning-search-backward

# HOME key
bindkey "^[[H" beginning-of-line
# END key
bindkey "^[[F" end-of-line
# DELETE key
bindkey "^[[3~" delete-char
# PAGE-UP key
bindkey "^[[5~" up-history
# PAGE-DOWN key
bindkey "^[[6~" down-history
# M-BACKSPACE
bindkey "^[^?" backward-delete-word
# M-DELETE
bindkey "^[[3;3~" delete-word
