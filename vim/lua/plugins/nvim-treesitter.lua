return {
    'nvim-treesitter/nvim-treesitter',
    lazy = true,
    build = ':TSUpdate',
    event = 'VimEnter *',
    dependencies = {
        'HiPhish/nvim-ts-rainbow2',
        'nvim-treesitter/nvim-treesitter-textobjects',
        'nvim-treesitter/nvim-treesitter-context',
        'm-demare/hlargs.nvim',
    },
    config = function()
        if vim.loop.os_uname().sysname == 'Windows_NT' then
            require('nvim-treesitter.install').compilers = { 'zig' }
        else
            require('nvim-treesitter.install').compilers = { 'clang' }
        end
        require('nvim-treesitter.configs').setup({
            highlight = { enable = true },
            rainbow = {
                enable = true,
                query = 'rainbow-parens',
                strategy = require('ts-rainbow').strategy.global,
            },
        })
        require('hlargs').setup()
    end,
}
