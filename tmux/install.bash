#!/usr/bin/env bash

ESC=$(printf '\033')

if ! type tmux &>/dev/null; then
  printf "${ESC}[31mSKIP: tmux is not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

if [ -e "${HOME}/.tmux.conf" ]; then
  rm "${HOME}/.tmux.conf"
fi
ln -snfv "${abs_script_dir}/tmux.conf" "${HOME}/.tmux.conf"

unset abs_script_dir
unset script_dir
