#!/usr/bin/env zsh

## Zinit Settings ##
if [[ -e ~/.zsh.d/.zinit/bin/zinit.zsh ]]; then
  source ~/.zsh.d/.zinit/bin/zinit.zsh
  autoload -Uz _zinit
  (( ${+_comps} )) && _comps[zinit]=_zinit
  PS1="%n@%m %~ %# "
  # zinit ice wait'!0' from"gl"; zinit load okutom/paradocs
  zinit ice wait'!0' from"gh-r" as"program"; zinit load junegunn/fzf-bin
  zinit ice wait'!0'; zinit load zsh-users/zsh-completions
  zinit ice wait'!0'; zinit load zsh-users/zsh-syntax-highlighting
  zinit ice wait lucid atload'_zsh_autosuggest_start'; zinit load zsh-users/zsh-autosuggestions
else
  echo "Cannot find zinit..."
fi
