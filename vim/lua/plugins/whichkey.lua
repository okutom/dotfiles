return {
    'folke/which-key.nvim',
    lazy = true,
    event = 'VeryLazy',
    opts = {
        win = {
            border = 'rounded',
        },
    },
    keys = {
        -- normal mode
        { '<leader>c', '', desc = '+comment' },
        { '<leader>f', '', desc = '+file' },
        { '<leader>fn', '<cmd>enew<cr>', desc = 'New File' },
        { '<leader>g', '', desc = '+git' },
        { '<leader>h', '', desc = '+harpoon' },
        { '<leader>m', '', desc = '+multicursors' },
        { '<leader>mm', '', desc = '+match' },
        { '<leader>q', '', desc = '+quit' },
        { '<leader>r', '', desc = '+refactor' },
        { '<leader>s', '', desc = '+search' },
        { '<leader>t', '', desc = '+telescope' },
        { '<leader>tf', '', desc = '+file pickers' },
        { '<leader>tg', '', desc = '+git' },
        { '<leader>tl', '', desc = '+lsp' },

        -- visual mode
        { '<leader>c', '', mode = 'v', desc = '+comment' },
        { '<leader>m', '', mode = 'v', desc = '+multicursors' },
        { '<leader>mm', '', mode = 'v', desc = '+match' },
        { '<leader>r', '', mode = 'v', desc = '+refactor' },
        { '<leader>s', '', mode = 'v', desc = '+search' },
    },
}
