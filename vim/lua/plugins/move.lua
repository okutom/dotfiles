return {
    'fedepujol/move.nvim',
    lazy = true,
    event = 'VeryLazy',
    config = function()
        vim.keymap.set({ 'n' }, '<A-t>', ':MoveLine(1)<CR>',
            { noremap = true, silent = true, desc = 'move down line' }
        )
        vim.keymap.set({ 'n' }, '<A-n>', ':MoveLine(-1)<CR>',
            { noremap = true, silent = true, desc = 'move up line' }
        )
        vim.keymap.set({ 'n' }, '<A-k>', ':MoveHChar(-1)<CR>',
            { noremap = true, silent = true, desc = 'move left char' }
        )
        vim.keymap.set({ 'n' }, '<A-s>', ':MoveHChar(1)<CR>',
            { noremap = true, silent = true, desc = 'move right char' }
        )

        vim.keymap.set({ 'v' }, '<A-t>', ':MoveBlock(1)<CR>',
            { noremap = true, silent = true, desc = 'move down block' }
        )
        vim.keymap.set({ 'v' }, '<A-n>', ':MoveBlock(-1)<CR>',
            { noremap = true, silent = true, desc = 'move up block' }
        )
        vim.keymap.set({ 'v' }, '<A-k>', ':MoveHBlock(-1)<CR>',
            { noremap = true, silent = true, desc = 'move left block' }
        )
        vim.keymap.set({ 'v' }, '<A-s>', ':MoveHBlock(1)<CR>',
            { noremap = true, silent = true, desc = 'move right block' }
        )
    end,
}
