#!/usr/bin/env zsh

umask 0022

if [[ -e "${HOME}/.profile" ]]; then
  source "${HOME}/.profile"
fi
