#!/bin/zsh

ss_dir=$HOME/Pictures/Screenshot/

if [[ ! -e $ss_dir ]]; then
  mkdir -p "$ss_dir"
fi

if [[ $1 = "-w" ]]; then
  scrot -q 100 -m -u Screenshot_%Y%m%d_%H%M%S.png -e 'mv $f ~/Pictures/Screenshot/ 2>/dev/null'
  #gnome-screenshot -w -f "$ss_dir"/Screenshot_$(date +%Y%m%d_%H%M%S).png
elif [[ $1 = "-a" ]]; then
  sleep 0.5 && scrot -q 100 -m -s Screenshot_%Y%m%d_%H%M%S.png -e 'mv $f ~/Pictures/Screenshot/ 2>/dev/null'
  #gnome-screenshot -a -f "$ss_dir"/Screenshot_$(date +%Y%m%d_%H%M%S).png
else
  scrot -q 100 -m Screenshot_%Y%m%d_%H%M%S.png -e 'mv $f ~/Pictures/Screenshot/ 2>/dev/null'
  #gnome-screenshot -f "$ss_dir"/Screenshot_$(date +%Y%m%d_%H%M%S).png
fi