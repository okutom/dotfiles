#!/usr/bin/env zsh

local nix_dir="${ZDOTDIR}/config/os/nixos"
for file in "${nix_dir}/"??*; do
  source "${file}"
done

alias nixos-update='(cd /etc/nixos && sudo nix flake update)'
alias hm-update='home-manager switch'

