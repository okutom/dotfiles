return {
    'filipdutescu/renamer.nvim',
    branch = 'master',
    lazy = true,
    event = 'VeryLazy',
    dependencies = 'nvim-lua/plenary.nvim',
    opts = {},
    keys = {
        {
            mode = { 'n', 'v' },
            '<leader>rn',
            function() require('renamer').rename() end,
            noremap = true,
            silent = true,
            desc = 'Rename symbol',
        },
        {
            mode = { 'i' },
            '<F2>',
            function() require('renamer').rename() end,
            noremap = true,
            silent = true,
            desc = 'Rename symbol',
        }
    },
}
