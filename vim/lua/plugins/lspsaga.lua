return {
    'nvimdev/lspsaga.nvim',
    dependencies = {
        'nvim-tree/nvim-web-devicons',
        'nvim-treesitter/nvim-treesitter',
    },
    lazy = true,
    event = 'LspAttach',
    opts = {},
}
