# my dotfiles  

## How to install  

### unix-like system

```sh
git clone https://gitlab.com/okutom/dotfiles.git ~/.dotfiles
~/.dotfiles/install.bash
```

### Windows

```powershell
git clone https://gitlab.com/okutom/dotfiles.git $HOME/.dotfiles
$HOME/.dotfiles/install.ps1
```
