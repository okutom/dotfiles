local awful = require("awful")
local gears = require("gears")

local client_keybinds = {}

client_keybinds.keys = gears.table.join(
    awful.key(
        { "Mod4", "Control" }, "Return",
        function(c) c:swap(awful.client.getmaster()) end,
        { description = "move to master", group = "client" }
    ),
    awful.key(
        { "Mod4" }, "q",
        function(c) c:kill() end,
        { description = "close", group = "client" }
    ),
    awful.key(
        { "Mod4", "Control" }, "space",
        function(c) awful.titlebar.toggle(c) end,
        { description = "toggle title bar", group = "client" }
    ),
    awful.key(
        { "Mod4" }, "F1",
        function(c)
            c.above = not c.above
            c:raise()
        end,
        { description = "toggle (un)above", group = "client" }
    ),
    awful.key(
        { "Mod4" }, "F2",
        function(c)
            c.ontop = not c.ontop
            c:raise()
        end,
        { description = "toggle (un)ontop", group = "client" }
    ),
    awful.key(
        { "Mod4" }, "F3",
        function(c)
            c.sticky = not c.sticky
        end,
        { description = "toggle (un)sticky", group = "client"}
    ),
    awful.key(
        { "Mod4" }, "F11",
        function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        { description = "toggle full screen", group = "client" }
    ),
    awful.key(
        { "Mod4" }, "`",
        function(c)
            c.floating = not c.floating
            c:raise()
        end,
        { description = "toggle (un)floating", group = "client" }
    ),
    awful.key(
        { "Mod4", }, "m",
        function(c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end,
        { description = "minimize", group = "client" }
    ),

    awful.key(
        { "Mod4" }, "b",
        function(c)
            c.maximized = not c.maximized
            c:raise()
        end,
        { description = "(un)maximize", group = "client" }
    ),
    awful.key(
        { "Mod4", "Control" }, "b",
        function(c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end,
        { description = "(un)maximize vertically", group = "client" }
    ),
    awful.key(
        { "Mod4", "Shift" }, "b",
        function(c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end,
        { description = "(un)maximize horizontally", group = "client" }
    ),

    awful.key(
        { "Mod4" }, "o",
        function(c) c:move_to_screen() end,
        { description = "move client to other screen", group = "screen" }
    )
)

client_keybinds.buttons = gears.table.join(
    awful.button(
        { }, 1,
        function(c) c:emit_signal("request::activate", "mouse_click", {raise = true}) end
    ),
    awful.button(
        { "Mod4" }, 1,
        function(c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
            awful.mouse.client.move(c)
        end
    ),
    awful.button(
        { "Mod4" }, 3,
        function(c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
            awful.mouse.client.resize(c)
        end
    )
)

return client_keybinds
