#!/usr/bin/env zsh

## ZSHOPTIONS ##
## Changing Directories
setopt auto_cd
setopt auto_pushd
setopt pushd_ignore_dups
setopt pushd_minus
setopt pushd_silent
## Completion
setopt always_to_end
setopt auto_list
#setopt glob_complete
setopt complete_in_word
setopt list_packed
unsetopt menu_complete
## Expansion and Globbing
setopt brace_ccl
unsetopt case_glob
setopt extended_glob
setopt glob_dots
#setopt glob_subst
#setopt ignore_braces
#unsetopt ksh_glob
#setopt magic_equal_subst
#setopt sh_glob
## History
setopt bang_hist
setopt extended_history
setopt hist_expire_dups_first
setopt hist_fcntl_lock
setopt hist_find_no_dups
setopt hist_ignore_all_dups
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_save_no_dups
setopt hist_verify
setopt inc_append_history
setopt share_history
## Initialisation
## Input/Output
setopt correct
unsetopt flow_control
setopt path_dirs
setopt rc_quotes
## Job Control
setopt auto_resume
unsetopt bg_nice
unsetopt check_jobs
unsetopt hup
setopt long_list_jobs
setopt notify
## Prompting
setopt prompt_subst
## Scripts and Functions
## Shell Emulation
## Shell State
## Zle
unsetopt beep
setopt combining_chars
