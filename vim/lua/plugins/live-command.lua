return {
    'smjonas/live-command.nvim',
    lazy = true,
    event = 'VeryLazy',
    config = function()
        require('live-command').setup({
            commands = {
                Norm = { cmd = 'norm' },
                D = { cmd = 'd' },
                G = { cmd = 'g' },
            },
        })
    end,
}
