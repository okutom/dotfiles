#!/usr/bin/env bash

ESC=$(printf '\033')

if ! type awesome &>/dev/null; then
  printf "${ESC}[31mSKIP: awesome is not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname ${0})
abs_script_dir=$(cd ${script_dir}; pwd)
config_dir="${HOME}/.config"

mkdir -p "${config_dir}"

[ -e "${config_dir}/awesome" ] && rm -rf "${config_dir}/awesome"
ln -snfv "${abs_script_dir}" "${config_dir}/awesome"

unset abs_script_dir
unset script_dir
