;;; function.el --- my function definition

;;; Commentary:

;;; Code:

(defun my-forward-char (arg &optional)
  "Move point to rignt for evil-mode.  If arg is specified, move ARG times."
  (interactive "p")
  (unless (eolp) (forward-char arg)))

(defun smart-beginning-of-line ()
  "Toggle begginning of sentence & line."
  (interactive)
  (let ((oldpos (point)))
    (back-to-indentation)
    (and (= oldpos (point))
         (beginning-of-line))))

(defun smart-open-line ()
  "Open line below and indent."
  (interactive)
  (move-end-of-line nil)
  (newline-and-indent))

(defun smart-open-line-above ()
  "Open line above and indent."
  (interactive)
  (save-excursion
    (beginning-of-line)
    (newline)
    (forward-line -1)
    (indent-for-tab-command)))

(provide 'function)

;;; function.el ends here

