return {
    'lukas-reineke/indent-blankline.nvim',
    main = 'ibl',
    lazy = true,
    event = 'VeryLazy',
    dependencies = {
        'nvim-treesitter/nvim-treesitter',
        'TheGLander/indent-rainbowline.nvim',
    },
    opts = function(_, opts)
        opts = {
            exclude = { filetypes = { 'dashboard' } },
        }
        return require('indent-rainbowline').make_opts(opts)
    end,
    -- config = function()
    --     local highlight = {
    --         'CursorColumn',
    --         'Whitespace',
    --     }
    --
    --     require('ibl').setup({
    --         exclude = { filetypes = { 'dashboard' } },
    --         indent = { highlight = highlight, char = '' },
    --         whitespace = {
    --             highlight = highlight,
    --             remove_blankline_trail = false,
    --         },
    --        scope = { enabled = false },
    --     })
    -- end,
}
