return {
    'cshuaimin/ssr.nvim',
    lazy = true,
    event = 'VeryLazy',
    config = function()
        require('ssr').setup({
            border = 'rounded',
            min_width = 50,
            min_height = 5,
            max_width = 120,
            max_heigt = 25,
            adjust_window = true,
            keymaps = {
                close = 'q',
                next_match = 'j',
                prev_match = 'J',
                replace_confirm = '<CR>',
                replace_all = '<C-CR>',
            },
        })
    end,
    keys = {
        {
            mode = { 'n', 'x' },
            '<leader>sr',
            function() require('ssr').open() end,
            desc = 'Structural Search & Replace',
        }
    },
}
