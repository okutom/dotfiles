#!/usr/bin/env zsh

if type doas &>/dev/null; then
  alias sudo='doas'
  alias sudoedit='doas "${EDITOR}"'
fi
