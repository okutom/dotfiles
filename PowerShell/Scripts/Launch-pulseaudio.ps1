$PulseaudioBin = "A:\App\pulseaudio\bin\pulseaudio.exe"
$TargetPid = Get-PID "pulseaudio"
$PulseaudioPidFile = "$HOME/.pulse/$HOST-runtime/pid"

if ($TargetPid -eq -1) {
    if (Test-Path $PulseaudioPidFile) {
        Remove-Item $PulseaudioPidFile
    }
    [System.Diagnostics.Process]::Start($PulseaudioBin)
}