-- delete
vim.keymap.set({ 'c', 'i' }, '<C-d>', '<Del>', { noremap = true })

-- paste
vim.keymap.set({ 'i' }, '<C-p>', '<Esc>pa', { noremap = true })
vim.keymap.set({ 'i' }, '<C-S-p>', '<Esc>Pa', { noremap = true })

-- beginning of sentence
vim.keymap.set({ 'n', 'v' }, '<C-i>', '^', { noremap = true })
-- vim.keymap.set('c', '<C-i>', '<Home>')
vim.keymap.set({ 'i' }, '<C-i>', '<Esc>I', { noremap = true })
-- beginning of line
vim.keymap.set({ 'n', 'v' }, '<C-S-i>', '0', { noremap = true })
--vim.keymap.set('c', '<C-S-i>', '<Home>')
vim.keymap.set({ 'i' }, '<C-S-i>', '<Esc>0i', { noremap = true })

-- end of line
vim.keymap.set({ 'n', 'v' }, '<C-a>', '$', { noremap = true })
vim.keymap.set({ 'c' }, '<C-a>', '<End>', { noremap = true })
vim.keymap.set({ 'i' }, '<C-a>', '<Esc>A', { noremap = true })

-- open line
-- below
vim.keymap.set({ 'n' }, '<C-CR>', 'mzo<Esc>`z', { noremap = true })
vim.keymap.set({ 'i' }, '<C-CR>', '<End><CR>', { noremap = true })
-- above
vim.keymap.set({ 'n' }, '<C-S-CR>', 'mzO<Esc>`z', { noremap = true })
vim.keymap.set({ 'i' }, '<C-S-CR>', '<Home><CR><Up>', { noremap = true })

-- turne off highlight
vim.keymap.set({ 'n' }, '<C-l>', ':nohlsearch<CR>', { noremap = true, silent = true })

-- indent
vim.keymap.set({ 'n' }, '<C-.>', '>>', { noremap = true })
vim.keymap.set({ 'n' }, '<C-,>', '<<', { noremap = true })
vim.keymap.set({ 'i' }, '<C-.>', '<C-t>', { noremap = true })
vim.keymap.set({ 'i' }, '<C-,>', '<C-d>', { noremap = true })

-- close
vim.keymap.set({ 'n' }, '<leader>qx', ':bp | bd #<CR>', { noremap = true, silent = true, desc = 'buffer delete' })
