return {
    'akinsho/toggleterm.nvim',
    version = '*',
    lazy = true,
    event = 'VeryLazy',
    config = function()
        local shell = vim.o.shell
        if os.getenv('OS') == 'Windows_NT' then
            shell = 'pwsh -NoLogo'
        end
        require('toggleterm').setup({
            shell = shell,
        })
    end,
    keys = {
        {
            mode = { 'n', 'i', 't', 'v' },
            '<A-4>',
            function()
                vim.api.nvim_exec('ToggleTerm', true)
            end,
            noremap = true,
            desc = 'toggle internal terminal',
        },
    },
}
