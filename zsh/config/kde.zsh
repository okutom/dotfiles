#!/usr/bin/env zsh

if [[ "${XDG_SESSION_DESKTOP}" != "KDE" ]]; then
  return
fi

if type ksshaskpass &>/dev/null; then
  export SSH_ASKPASS=$(which ksshaskpass)
  export SSH_ASKPASS_REQUIRE=prefer

  export GIT_ASKPASS=$(which ksshaskpass)
fi
