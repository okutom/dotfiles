#!/usr/bin/env bash

ESC=$(printf '\033')

if ! type picom &>/dev/null; then
  printf "${ESC}[31mSKIP: picom is not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname ${0})
abs_script_dir=$(cd ${script_dir}; pwd)
config_dir="${HOME}/.config"

[ -e "${config_dir}/picom" ] && rm -rf "${config_dir}/picom"
ln -snfv "${abs_script_dir}/picom" "${config_dir}/picom"

unset abs_script_dir
unset script_dir
