return {
    'yamatsum/nvim-cursorline',
    lazy = true,
    event = 'VeryLazy',
    opts = {
        cursorline = {
            enable = true,
            timeout = 0,
            number = false,
        },
        cursorword = {
            enable = true,
            min_length = 2,
            hl = { underline = true },
        },
    },
}
