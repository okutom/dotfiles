return {
    'stevearc/conform.nvim',
    lazy = true,
    event = 'BufEnter',
    opts = {
        format_on_save = {
            timeout_ms = 500,
            lsp_fallback = true,
        },
    },
}
