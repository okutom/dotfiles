#!/usr/bin/env zsh

if [[ -e ~/.cargo/bin/cargo-generate ]]; then
  alias cargogenac='cargo generate --git https://github.com/rust-lang-ja/atcoder-rust-base --branch ja'
fi
