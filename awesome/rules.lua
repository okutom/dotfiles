local awful = require("awful")
local beautiful = require("beautiful")
local client_keybinds = require("client_keybinds")

local rules = {
    -- All clients will match this rule.
    {
        rule = { },
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = client_keybinds.keys,
            buttons = client_keybinds.buttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap + awful.placement.no_offscreen
        }
    },

    -- Floating clients.
    {
        rule_any = {
            class = {
                "Arandr",
                "Blueman-manager",
                "Wpa_gui",
            },
            name = {
                "Event Tester", -- xev.
                "Picture in Picture",
                "ピクチャー イン ピクチャー",
                "音量調節",
            },
            role = {
                "pop-up",
            }
        },
        properties = { floating = true }
    },

    -- Add titlebars to normal clients and dialogs
    {
        rule_any = { type = { "normal", "dialog" } },
        properties = { titlebars_enabled = false }
    },
}

return rules
