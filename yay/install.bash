#!/usr/bin/env bash

ESC=$(printf '\033')

if ! type yay &>/dev/null; then
  printf "${ESC}[31mSKIP: yay is not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

mkdir -p "${HOME}/.config/yay"
ln -snfv "${abs_script_dir}/config.json" "${HOME}/.config/yay/config.json"

unset abs_script_dir
unset script_dir
