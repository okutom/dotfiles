#!/usr/bin/env zsh

## Alias ##
alias sudo='sudo '
alias diff='diff --color=auto'
alias dir='dir --color=auto'
alias grep='grep --color=auto'
alias ip='ip -color=auto'
alias vdir='vdir --color=auto'
if type eza &>/dev/null; then
    alias ls='eza -F --color=auto --group-directories-first'
    alias la='eza -F -a --color=auto --group-directories-first'
    alias ll='eza -F -b -g -l --color=auto --group-directories-first'
    alias lla='eza -F -a -b -g -l --color=auto --group-directories-first'
    alias lal='eza -F -a -b -g -l --color=auto --group-directories-first'
    alias tree='eza -F -T --icons --color=auto --group-directories-first'
else
    alias ls='ls -Fv --color=auto --group-directories-first'
    alias la='ls -A'
    alias ll='ls -Hhl'
    alias lla='ls -AHhl'
    alias lal='ls -AHhl'
fi
if type bat &>/dev/null; then
    export BAT_THEME="Monokai Extended"
    alias cat='bat'
fi
export PAGER='less'
export LESS='FgiMRSwX --use-color -Dd+r$Du+b'

