#!/usr/bin/env bash

ESC=$(printf '\033')

if ! type alacritty &>/dev/null; then
  printf "${ESC}[31mSKIP: alacritty is not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)
font_name=`fc-list|grep -i udevgothicnflg|grep -i regular|cut -d : -f 2|xargs`

mkdir -p "${HOME}"/.config/alacritty
ln -snfv "${abs_script_dir}"/alacritty.toml "${HOME}"/.config/alacritty/alacritty.toml
ln -snfv "${abs_script_dir}"/color-monokai.toml "${HOME}"/.config/alacritty/color-monokai.toml
#ln -snfv "${abs_script_dir}"/keybindings.toml "${HOME}"/.config/alacritty/keybindings.toml
sed -e "s/__FONT_NAME__/${font_name}/g" "${abs_script_dir}"/font.toml > "${HOME}"/.config/alacritty/font.toml

unset font_name
unset abs_script_dir
unset script_dir
