-- Standard awesome library
local awful = require("awful")
require("awful.autofocus")
local beautiful = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")
local wibox = require("wibox")
-- My widget
local audio = require("widget.audio")
local datetime = require("widget.datetime")
local logout = require("widget.logout")
-- My library
local autostart = require("autostart")
local global_keybinds = require("global_keybinds")
local my_rules = require("rules")
local utils = require("utils")


-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Oops, there were errors during startup!",
        text = awesome.startup_errors
    })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function(err)
        -- Make sure we don't go into an endless error loop
        if in_error then
            return
        end
        in_error = true

        naughty.notify({
            preset = naughty.config.presets.critical,
            title = "Oops, an error happened!",
            text = tostring(err)
        })
        in_error = false
    end)
end
-- }}}

-- {{{ Notification sound
--function naughty.config.notify_callback(args)
    --awful.spawn.with_shell("vlc --intf dummy " .. "~/test.flac" .. " --start-time=20 --stop-time=23 vlc://quit")
    --return args
--end
-- }}}


-- {{{ Run garbage collector regularly to prevent memory leaks
gears.timer {
    timeout = 30,
    autostart = true,
    callback = function() collectgarbage() end
}
-- }}}

-- {{{ Setup theme
beautiful.init(gears.filesystem.get_configuration_dir() .. "/theme/theme.lua")
-- }}}

-- {{{ Layouts
awful.layout.layouts = {
    awful.layout.suit.tile,
    --awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    --awful.layout.suit.title.top,
    awful.layout.suit.fair,
    --awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    awful.layout.suit.floating,
    --awful.layout.suit.max,
    awful.layout.suit.magnifier,
    awful.layout.suit.max.fullscreen,
    --awful.layout.suit.corner.nw,
    --awful.layout.suit.corner.ne,
    --awful.layout.suit.corner.sw,
    --awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Taskbar
-- Create a wibox for each screen and add it
awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    utils.set_wallpaper(s)

    -- Each screen has its own tag tabl.
    awful.tag({ "✿", "✿", "✿", "✿", "✿", "✿", "✿", "✿", "✿" }, s, awful.layout.layouts[1])

    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.layout_box = awful.widget.layoutbox(s)
    s.layout_box:buttons(global_keybinds.layout_buttons)

    -- Create a tag list widget
    s.tag_list = awful.widget.taglist {
        screen = s,
        filter = awful.widget.taglist.filter.all,
        buttons = global_keybinds.tag_buttons
    }

    -- Create a task list widget
    s.task_list = awful.widget.tasklist {
        screen = s,
        filter = awful.widget.tasklist.filter.currenttags,
        buttons = global_keybinds.task_buttons
    }

    -- Create the wibox
    s.wb = awful.wibar({ position = "top", height = 25, screen = s })
    -- Add widgets to the wibox
    s.wb:setup {
        layout = wibox.layout.align.horizontal(),
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal(),
            s.layout_box,
            s.tag_list,
        },
        -- {-- Middle widget
        s.task_list,
        -- },
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal(),
            wibox.widget.systray(),
            audio.applet(),
            datetime.applet(),
            logout.applet(),
        },
    }
end)
-- }}}

-- {{{ Set global key & mouse bindings
root.keys(global_keybinds.keys)
root.buttons(global_keybinds.buttons)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = my_rules
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- awful.client.setslave(c)

    if awesome.startup
        and not c.size_hints.user_position
        and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal(
    "request::titlebars", function(c)
        -- buttons for the titlebar
        local buttons = gears.table.join(
            awful.button(
                { }, 1,
                function()
                    c:emit_signal("request::activate", "titlebar", {raise = true})
                    awful.mouse.client.move(c)
                end
            ),
            awful.button(
                { }, 3,
                function()
                    c:emit_signal("request::activate", "titlebar", {raise = true})
                    awful.mouse.client.resize(c)
                end
            )
        )

        awful.titlebar(c):setup {
            { -- Left
                awful.titlebar.widget.closebutton(c),
                awful.titlebar.widget.minimizebutton(c),
                awful.titlebar.widget.maximizedbutton(c),
                -- awful.titlebar.widget.floatingbutton(c),
                -- awful.titlebar.widget.ontopbutton(c),
                -- awful.titlebar.widget.stickybutton(c),
                layout  = wibox.layout.fixed.horizontal
            },
            {
                awful.titlebar.widget.iconwidget(c),
                buttons = buttons,
                layout  = wibox.layout.fixed.horizontal
            },
            { -- Middle
                { -- Title
                    align  = "center",
                    widget = awful.titlebar.widget.titlewidget(c)
                },
                buttons = buttons,
                layout  = wibox.layout.flex.horizontal
            },
            layout = wibox.layout.fixed.horizontal
        }
    end
)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal(
    "mouse::enter",
    function(c) c:emit_signal("request::activate", "mouse_enter", { raise = false }) end
)

client.connect_signal(
    "focus",
    function(c) c.border_color = beautiful.border_focus end
)
client.connect_signal(
    "unfocus",
    function(c) c.border_color = beautiful.border_normal end
)

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", utils.set_wallpaper)
-- }}}

-- {{{ Auto start
autostart.run()
-- }}}
