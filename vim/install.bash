#!/usr/bin/env bash

ESC=$(printf '\033')

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

if ! type nvim &>/dev/null; then
  printf "${ESC}[31mSKIP: neovim is not installed.${ESC}[m\n"
  exit
else
  confdir=${XDG_CONFIG_HOME:-${HOME}/.config}
  mkdir -p "${confdir}"
  ln -sfv "${abs_script_dir}" "${confdir}/nvim"
fi

if ! type vim &>/dev/null; then
  printf "${ESC}[31mSKIP: vim is not installed.${ESC}[m\n"
  exit
else
  ln -sfv "${abs_script_dir}/vimscript/init.vim" "${HOME}/.vimrc"
fi

unset confdir
unset abs_script_dir
unset script_dir
