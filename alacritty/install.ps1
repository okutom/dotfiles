If(!(Test-Path "$Env:PROGRAMFILES/Alacritty/alacritty.exe")) {
    Write-Host -ForegroundColor Red "Alacritty is not installed."
    exit 1
}

$script_dir = Split-Path $MyInvocation.MyCommand.Path
$config_dir = "$Env:AppData/alacritty"

If(Test-Path "$config_dir") {
    Remove-Item -Path "$config_dir" -Recurse -Force
}

New-Item -ItemType Directory -Path "$Env:AppData/" -Name "alacritty"
New-Item -ItemType SymbolicLink -Value "$script_dir/alacritty.toml" "$config_dir/alacritty.toml"
New-Item -ItemType SymbolicLink -Value "$script_dir/color-monokai.toml" "$config_dir/color-monokai.toml"

(Get-Content "$script_dir/font.toml") | foreach { $_ -replace "__FONT_NAME__", "UDEV Gothic NFLG" } | Set-Content "$config_dir/font.toml"

$dist_name = (wsl --status)[0].Split(' ')[1] -replace ([char](0x00), '')
(Get-Content "$script_dir/windows.toml") | foreach { $_ -replace "__DIST_NAME__", "$dist_name" } | Set-Content "$config_dir/windows.toml"