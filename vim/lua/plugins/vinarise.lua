return {
    enabled = false,
    'Shougo/vinarise.vim',
    lazy = true,
    event = 'VeryLazy',
}
