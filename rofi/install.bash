#!/usr/bin/env bash

ESC=$(printf '\033')

if ! type rofi &>/dev/null; then
  printf "${ESC}[31mSKIP: rofi is not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname ${0})
abs_script_dir=$(cd ${script_dir}; pwd)
config_dir="${HOME}/.config"

[ -e "${config_dir}/rofi" ] && rm -rf "${config_dir}/rofi"
ln -snfv "${abs_script_dir}/rofi" "${config_dir}/rofi"

unset abs_script_dir
unset script_dir
