#!/usr/bin/env bash

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

if [ -e "${HOME}/.gitconfig" ]; then
  rm -f "${HOME}/.gitconfig"
fi
ln -snfv "${abs_script_dir}/gitconfig" "${HOME}/.gitconfig"

unset abs_script_dir
unset script_dir
