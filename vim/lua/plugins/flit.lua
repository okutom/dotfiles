return {
    'ggandor/flit.nvim',
    dependencies = {
        'ggandor/leap.nvim',
        lazy = true,
        event = 'BufEnter',
        config = function()
            local leap = require('leap')
            leap.opts.safe_labels = {
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
                'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
                'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                'y', 'z',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                'Y', 'Z',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            }
            leap.opts.labels = {
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
                'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
                'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                'y', 'z',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                'Y', 'Z',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            }
        end,
        keys = {
            {
                mode = { 'n' },
                ';',
                function()
                    require('leap').leap({ target_windows = { vim.fn.win_getid() } })
                end,
                noremap = true,
                desc = 'search whole buffer',
            },
            {
                mode = { 'n' },
                '<C-;>',
                function()
                    require('leap').leap({
                        target_windows = vim.tbl_filter(
                        function(win)
                            return vim.api.nvim_win_get_config(win).focusable
                        end,
                        vim.api.nvim_tabpage_list_wins(0)
                        )
                    })
                end,
                noremap = true,
                desc = 'search whole window',
            }
        },
    },
    lazy = true,
    event = 'BufEnter',
    config = function()
        require('flit').setup({
            keys = { f = 'f', F = 'F', t = 'l', T = 'L' },
            labeled_modes = 'v',
            multiline = true,
            opts = {}
        })
    end,
}
