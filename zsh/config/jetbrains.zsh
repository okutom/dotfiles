#!/usr/bin/env zsh

if [[ -e "${HOME}/.local/share/Jetbrains/Toolbox/scripts" ]]; then
  export PATH="${PATH}:${HOME}/.local/share/JetBrains/Toolbox/scripts"
fi
