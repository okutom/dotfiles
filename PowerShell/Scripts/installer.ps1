# 必要な行のコメントを外して実行する

# 必須ソフトウェア
# winget install Microsoft.Powershell
# winget install Microsoft.PowerToys
# winget install Git.Git
# winget install VivaldiTechnologies.Vivaldi
# winget install AdGuard.AdGuard
# winget install Starship.Starship
# winget install Google.JapaneseIME
# winget install pCloudAG.pCloudDrive

# 場合により必要なソフトウェア
# winget install 7zip.7zip
# winget install VideoLAN.VLC
# winget install RedHat.Podman
# winget install jurplel.qView
# winget install SublimeHQ.SublimeText.4
# winget install JetBrains.Toolbox
# winget install OBSProject.OBSStudio
# winget install marha.VcXsrv
# winget install JanDeDobbeleer.OhMyPosh
# winget install Amazon.Kindle

# ゲーム
# winget install GOG.Galaxy
# winget install Valve.Steam
# winget install EpicGames.EpicGamesLauncher
# winget install Ubisoft.Connect
# winget install Amazon.Games
# winget install Riot Game league_of_legends.live
# winget install Discord.Discord