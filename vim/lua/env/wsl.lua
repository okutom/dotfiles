if vim.fn.executable('win32yank.exe') == 0 then
    print("win32yank.exe not found, clipboard integration won't work")
-- else
--     vim.g.clipboard = {
--         name = 'win32yank',
--         copy = {
--             ['+'] = '~/.local/bin/win32yank.exe -i --crlf',
--             ['*'] = '~/.local/bin/win32yank.exe -i --crlf'
--         },
--         paste = {
--             ['+'] = '~/.local/bin/win32yank.exe -o --lf',
--             ['*'] = '~/.local/bin/win32yank.exe -o --lf',
--         },
--         cach_enabled = 0,
--     }
end
