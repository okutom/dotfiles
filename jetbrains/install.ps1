If(!(Test-Path "$HOME/AppData/Local/JetBrains")) {
    Write-Host -ForegroundColor Red "Any JetBrains tool are not installed."
    exit 1
}

$script_dir = Split-Path $MyInvocation.MyCommand.Path

New-Item -ItemType SymbolicLink -Value "$script_dir/ideavimrc" "$HOME/.ideavimrc" -Force
Set-ItemProperty -Path "$HOME/.ideavimrc" -Name Attributes -value 'Hidden'
