return {
    'ThePrimeagen/harpoon',
    branch = 'harpoon2',
    lazy = true,
    event = 'VeryLazy',
    dependencies = 'nvim-lua/plenary.nvim',
    config = function()
        local harpoon = require('harpoon')
        harpoon:setup()
    end,
    keys = {
        {
            mode = { 'n' },
            '<leader>ha',
            function()
                local harpoon = require('harpoon')
                harpoon:setup()
                harpoon:list():append()
            end,
            desc = 'append'
        }
    },
}
