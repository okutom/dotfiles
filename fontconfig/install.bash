#!/usr/bin/env bash

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

if [ -e "${HOME}/.config/fontconfig/fonts.conf" ]; then
  cp "${HOME}/.config/fontconfig/fonts.conf" "${HOME}/.config/fontconfig/fonts.conf.bak"
  sed -i '/<\/fontconfig>/i <include ignore_missing="yes">~/.dotfiles/fontconfig/fonts.conf<\/include>' "${HOME}/.config/fontconfig/fonts.conf"
else
  mkdir -p "${HOME}/.config/fontconfig"
  cp "${script_dir}/fonts.conf" "${HOME}/.config/fontconfig/fonts.conf"
fi

unset abs_script_dir
unset script_dir
