#!/usr/bin/env bash

ESC=$(printf '\033')

if [ ! -e "${HOME}/.local/share/JetBrains" ]; then
  printf "${ESC}[31mSKIP: Any JetBrains tool are not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

ln -snfv "${abs_script_dir}/ideavimrc" "${HOME}/.ideavimrc"

unset abs_script_dir
unset script_dir
