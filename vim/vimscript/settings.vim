""" vim settings
set clipboard+=unnamed
set hlsearch
set ignorecase
set incsearch
set notimeout
set number
set relativenumber
set showmode
set smartcase
set visualbell

""" indent
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4
set autoindent
set smartindent
