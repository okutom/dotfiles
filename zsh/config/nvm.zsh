#!/usr/bin/env zsh

temp_nvm_dir="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"

if [[ -e "${temp_nvm_dir}" ]]; then
  export NVM_DIR="${temp_nvm_dir}"
  if [[ -s "${NVM_DIR}/nvm.sh" ]]; then
    source "${NVM_DIR}/nvm.sh"
  fi
fi

unset temp_nvm_dir

