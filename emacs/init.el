;;; init.el --- My init.el  -*- lexical-binding: t; -*-

;;; Commentary:

;; コードの検索にはimenuを使うと良い

;;; Code:

(eval-and-compile
  (when (or load-file-name byte-compile-current-file)
    (setq user-emacs-directory
          (expand-file-name
           (file-name-directory (or load-file-name byte-compile-current-file))))))

(eval-and-compile
  (customize-set-variable
   'package-archives '(("gnu"   . "https://elpa.gnu.org/packages/")
                       ("melpa" . "https://melpa.org/packages/")
                       ("org"   . "https://orgmode.org/elpa/")))
  (package-initialize)
  (unless (package-installed-p 'leaf)
    (package-refresh-contents)
    (package-install 'leaf))

  (leaf leaf-keywords
    :ensure t
    :init
    (leaf hydra :ensure t)
    (leaf el-get :ensure t)
    (leaf blackout :ensure t)
    :config
    (leaf-keywords-init)))

;; builtin packages
(leaf cus-edit
  :doc "tools for customizing Emacs and Lisp packages"
  :tag "builtin" "faces" "help"
  :custom `((custom-file . ,(locate-user-emacs-file "custom.el"))))

(leaf cus-start
  :doc "define customization properties of builtins"
  :tag "builtin" "internal"
  :preface
  (defun c/redraw-frame nil
    (interactive)
    (redraw-frame))
  :custom '((user-full-name . "okutom")
            (user-mail-address . "3932906-okutom@users.noreply.gitlab.com")
            (user-login-name . "okutom")
            (create-lockfiles . nil)
            (debug-on-error . t)
            (init-file-debug . t)
            (frame-resize-pixelwise . t)
            (enable-recursive-minibuffers . t)
            (history-length . 1000)
            (history-delete-duplicates . t)
            (scroll-preserve-screen-position . t)
            (scroll-conservatively . 100)
            (mouse-wheel-scroll-amount . '(1 ((control) . 5)))
            (ring-bell-function . 'ignore)
            (text-quoting-style . 'straight)
            (truncate-lines . t)
            (use-dialog-box . nil)
            (use-file-dialog . nil)
            (menu-bar-mode . t)
            (tool-bar-mode . nil)
            (scroll-bar-mode . nil)
            (indent-tabs-mode . nil)
            (blink-cursor-mode . nil))
  :config
  (defalias 'yes-or-no-p 'y-or-n-p)
  (keyboard-translate ?\C-h ?\C-?))

(leaf autorevert
  :doc "revert buffers when files on disk change"
  :tag "builtin"
  :custom ((auto-revert-interval . 1))
  :global-minor-mode global-auto-revert-mode)

(leaf cc-mode
  :doc "major mode for editing C and similar languages"
  :tag "builtin"
  :defvar (c-basic-offset)
  :bind (c-mode-base-map
         ("C-c c" . compile))
  :mode-hook
  (c-mode-hook . ((c-set-style "bsd")
                  (setq c-basic-offset 4)))
  (c++-mode-hook . ((c-set-style "bsd")
                    (setq c-basic-offset 4))))

(leaf delsel
  :doc "delete selection if you insert"
  :tag "builtin"
  :global-minor-mode delete-selection-mode)

(leaf paren
  :doc "highlight matching paren"
  :tag "builtin"
  :custom (show-paren-delay . 0.1)
  :global-minor-mode show-paren-mode
  :custom-face (show-paren-match . '((t (:background "#A9DC76")))))

(leaf electric-pair
  :tag "builtin"
  :hook (prog-mode-hook . electric-pair-mode))

(leaf simple
  :doc "basic editing commands for Emacs"
  :tag "builtin" "internal"
  :custom ((kill-ring-max . 100)
           (kill-read-only-ok . t)
           (kill-whole-line . t)
           (eval-expression-print-length . nil)
           (eval-expression-print-level . nil)))

(leaf files
  :doc "file input and output commands for Emacs"
  :tag "builtin"
  :custom `((auto-save-timeout . 15)
            (auto-save-interval . 60)
            ;; (auto-save-file-name-transforms . '((".*" ,(locate-user-emacs-file "backup/") t)))
            ;; (backup-directory-alist . '((".*" . ,(locate-user-emacs-file "backup"))
            ;;                             (,tramp-file-name-regexp . nil)))
            (version-control . t)
            (delete-old-versions . t)))

(leaf startup
  :doc "process Emacs shell arguments"
  :tag "builtin" "internal"
  ;; :custom `((auto-save-list-file-prefix . ,(locate-user-emacs-file "backup/.saves-")))
  )

(leaf whitespace
  :tag "builtin"
  :custom (whitespace-style . '(tabs tab-mark trailing lines-tail empty))
  :global-minor-mode global-whitespace-mode)

(leaf display-line-numbers
  :tag "builtin"
  :custom (display-line-numbers-type . 'relative)
  :hook (prog-mode-hook . display-line-numbers-mode))

(leaf hs-minor-mode
  :tag "builtin"
  :hook (prog-mode-hook . hs-minor-mode))

(leaf savehist
  :tag "builtin"
  :hook (after-init-hook . savehist-mode))

(leaf emacs
  :tag "builtin"
  :load-path "~/.emacs.d/lisp/"
  :require function
  :defun (smart-open-line smart-open-line-above)
  :custom ((completion-cycle-threshold . 3)
           (tab-always-indent . 'complete))
  :bind (("C-<return>" . smart-open-line)
         ("C-S-<return>" . smart-open-line-above)))

;; other packages
(leaf auto-package-update
  :ensure t
  :custom (auto-package-update-interval . 1)
  :config
  (auto-package-update-maybe))

(leaf super-save
  :ensure t
  :hook (after-init-hook . super-save-mode)
  :custom ((super-save-auto-save-when-idle . t)
           (super-save-remote-files . nil)))

(leaf no-littering
  :ensure t
  :require t
  :config
  (no-littering-theme-backups))

(leaf font
  :config
  (add-to-list 'default-frame-alist '(font . "UDEV Gothic NFLG-12"))
  (leaf nerd-icons :ensure t)
  (leaf text-scale
    :bind ("C-0" . hydra-text-scale/body)
    :config
    :hydra (hydra-text-scale (:hint nil)
                             "
    ^zoom IN^   ^zoom OUT^   ^Reset^
    ^^^^^^---------------------------
    _+_          _-_        _0_
    _=_          ___
    "
                             ("+" text-scale-increase)
                             ("=" text-scale-increase)
                             ("-" text-scale-decrease)
                             ("_" text-scale-decrease)
                             ("0" (text-scale-adjust 0))
                             ("q" nil "quit" :exit t)))
  (leaf ligature
    :ensure t
    :hook (prog-mode-hook . ligature-mode)
    :config
    (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                         ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                         "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                         "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                         "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                         "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                         "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                         "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                         ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                         "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                         "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                         "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                         "\\\\" "://")))
  (leaf emojify
    :ensure t
    :hook (after-init-hook . global-emojify-mode)))

(leaf tr-ime
  :if (eq system-type 'windows-nt)
  :ensure t
  :setq-default (w32-ime-mode-line-state-indicator . "[--]")
  :custom ((default-input-method . "W32-IME")
           (w32-ime-mode-line-state-indicator-list . '("[--]" "[あ]" "[--]"))
           (w32-ime-buffer-switch-p . nil))
  :config
  (tr-ime-advanced-install)
  (w32-ime-initialize)
  (w32-ime-wrap-function-to-control-ime 'universal-argument)
  (w32-ime-wrap-function-to-control-ime 'read-string)
  (w32-ime-wrap-function-to-control-ime 'read-char)
  (w32-ime-wrap-function-to-control-ime 'read-from-minibuffer)
  (w32-ime-wrap-function-to-control-ime 'y-or-n-p)
  (w32-ime-wrap-function-to-control-ime 'yes-or-no-p)
  (w32-ime-wrap-function-to-control-ime 'map-y-or-n-p)
  (w32-ime-wrap-function-to-control-ime 'register-read-with-preview))

(leaf flycheck
  :doc "On-the-fly syntax checking"
  :req "dash-2.12.1" "pkg-info-0.4" "let-alist-1.0.4" "seq-1.11" "emacs-24.3"
  :tag "minor-mode" "tools" "languages" "convenience" "emacs>=24.3"
  :url "http://www.flycheck.org"
  :emacs>= 24.3
  :ensure t
  :bind (("M-C-t" . flycheck-next-error)
         ("M-C-n" . flycheck-previous-error))
  :global-minor-mode global-flycheck-mode)

(leaf dashboard
  :ensure t
  :custom (display-line-numbers . nil) ; 行番号が表示されることがあったため非表示を明示する
  :config
  (dashboard-setup-startup-hook))

(leaf volatile-highlights
  :ensure t
  :blackout t
  :defun (vhl/define-extension vhl/install-extension)
  :hook (after-init-hook . volatile-highlights-mode)
  :config
  (vhl/define-extension 'evil
                        'evil-paste-after
                        'evil-paste-before
                        'evil-paste-pop
                        'evil-move
                        'hideshow
                        'hs-hide-block
                        'hs-show-block)
  (vhl/install-extension 'evil)
  (vhl/install-extension 'hideshow))

(leaf centaur-tabs
  :ensure t
  :leaf-defer nil
  :defun (centaur-tabs-headline-match centaur-tabs-change-fonts)
  :hook (dired-mode . centaur-tabs-local-mode)
  :custom ((centaur-tabs-style . "alternate")
           (centaur-tabs-set-icons . t)
           (centaur-tabs-set-bar . 'under)
           (x-underline-at-descent-line . t)
           (centaur-tabs-set-modified-marker . t)
           (centaur-tabs-modified-marker . "●")
           (centaur-tabs-cycle-scope . 'tabs))
  :config
  (centaur-tabs-mode t)
  (centaur-tabs-headline-match)
  (centaur-tabs-change-fonts "UDEV Gothic 35NFLG" 110)
  :bind (("C-<tab>" . centaur-tabs-forward)
         ("C-S-<tab>" . centaur-tabs-backward)))

(leaf ace-jump-mode
  :ensure t)

(leaf dumb-jump
  :ensure t)

(leaf anzu
  :ensure t
  :blackout t
  :global-minor-mode global-anzu-mode)

(leaf minimap
  :el-get samrjack/minimap
  :blackout t
  :global-minor-mode minimap-mode
  ;; :hook (prog-mode-hook . minimap-mode) ; <- This way is buggy
  :custom-face
  (minimap-active-region-background . '((t (:background "#75715E"))))
  :custom ((minimap-window-location . 'right)
           (minimap-minimum-width . 20)
           (minimap-maximum-width . 20)
           (minimap-update-delay . 0.1)))

(leaf doom-themes
  :ensure t
  :custom ((doom-themes-enable-bold . t)
           (doom-themes-enable-italic . t))
  :config
  (load-theme 'doom-monokai-classic t)
  
  (leaf doom-modeline
    :ensure t
    :init (doom-modeline-mode 1)
    :custom ((doom-modeline-enable-word-count . t)
             (doom-modeline-indent-info . t))))

(leaf highlight-indent-guides
  :ensure t
  :hook (prog-mode-hook . highlight-indent-guides-mode)
  :custom ((highlight-indent-guides-method . 'fill)
           (highlight-indent-guides-auto-enabled . t)
           (highlight-indent-guides-responsive . t)))

(leaf aggressive-indent
  :ensure t
  :global-minor-mode global-aggressive-indent-mode)

(leaf idle-highlight-mode
  :ensure t
  :hook ((prog-mode-hook . idle-highlight-mode)
         (text-mode-hook . idle-highlight-mode))
  :custom (idle-highlight-idle-time . 0.1))

(leaf rainbow-mode
  :ensure t
  :hook (prog-mode-hook . rainbow-mode))

(leaf smart-hungry-delete
  :ensure t)

(leaf multiple-cursors
  :disabled t
  :ensure t
  :bind (("M-<down-mouse-1>" . nil)
         ("M-<mouse-1>" . mc/add-cursor-on-click)))

(leaf rainbow-delimiters
  :ensure t
  :hook (prog-mode-hook . rainbow-delimiters-mode))

(leaf move-text
  :ensure t
  :bind (("M-t" . move-text-down)
         ("M-<down>" . move-text-down)
         ("M-n" . move-text-up)
         ("M-<up>" . move-text-up)))

(leaf undo-fu
  :ensure t
  :bind (("C-z" . undo-fu-only-undo)
         ("C-S-Z" . undo-fu-only-redo)))

(leaf vundo
  :ensure t
  :bind (("M-u" . vundo)
         (vundo-mode-map
          ("k" . vundo-backward)
          ("t" . vundo-next)
          ("n" . vundo-previous)
          ("s" . vundo-forward)
          ("i" . vundo-stem-root)
          ("a" . vundo-stem-end)
          ("<f1>" . vundo--inspect))))

(leaf yasnippet
  :ensure t
  :hook (prog-mode-hook . yas-global-mode)
  :config
  (leaf yasnippet-snippets
    :ensure t))

(leaf evil-nerd-commenter
  :ensure t
  :bind ("M-;" . evilnc-comment-or-uncomment-lines))

(leaf format-all
  :ensure t
  :bind ("C-S-M-l" . format-all-buffer))

(leaf projectile
  :ensure t
  :hook (after-init-hook . projectile-mode))

(leaf vertico
  :ensure t
  :init (vertico-mode)
  :custom (vertico-cycle . t)
  :bind (vertico-map
         ("C-t" . vertico-next)
         ("C-n" . vertico-previous)))

(leaf consult
  :ensure t
  :custom ((register-preview-delay . 0)
           (register-preview-function . #'consult-register-format))
  :init
  (advice-add #'register-preview :override #'consult-register-window))

(leaf marginalia
  :ensure t
  :init (marginalia-mode))

(leaf orderless
  :ensure t
  :custom ((completion-styles . '(orderless basic))
           (completion-category-defaults . nil)
           (completion-category-owerrides . '((file (styles partial-completion))))))

(leaf prescient
  :ensure t
  :config
  (leaf vertico-prescient
    :ensure t
    :after (vertico prescient)
    :config (vertico-prescient-mode)))

(leaf magit
  :ensure t
  :bind ("M-g" . magit-status)
  :config
  (leaf diff-hl
    :ensure t
    :hook ((magit-pre-refresh-hook . diff-hl-magit-pre-refresh)
           (magit-post-refresh-hook . diff-hl-magit-post-refresh))
    :global-minor-mode global-diff-hl-mode)

  (leaf blamer
    :emacs>= 27.1
    :ensure t
    :custom ((blamer-idle-time . 0.3)
             (blamer-min-offset . 70))
    :global-minor-mode global-blamer-mode)
  )

(leaf company
  :ensure t
  :global-minor-mode global-company-mode
  :custom ((company-idle-delay . 0)
           (company-minimum-prefix-length . 1)
           (company-transformers . '(company-sort-by-occurrence)))
  :bind ((company-active-map
          ("C-t" . company-select-next)
          ("C-n" . company-select-previous)
          ("<tab>" . company-complete-selection))
         (company-search-map
          ("C-t" . company-select-next)
          ("C-n" . company-select-previous)))
  :config
  (leaf company-quickhelp
    :ensure t
    :after company
    :hook (global-company-mode-hook . company-quickhelp-mode))

  (leaf company-statistics
    :ensure t
    :after company
    :hook (global-company-mode-hook . company-statistics-mode))

  (leaf company-emoji
    :disabled t
    :ensure t
    :after company
    :require t
    :config
    (add-to-list 'company-backends 'company-emoji))

  (leaf company-emojify
    :disabled t
    :emacs>= 24.3
    :ensure t
    :after company
    :require t
    :custom (company-emojify-insert-unicode . t)
    :config
    (add-to-list 'company-backends 'company-emojify))

  (leaf company-posframe
    :ensure t
    :after company
    :require t
    :hook (global-company-mode-hook . company-posframe-mode))
  )

;; languages
(leaf web-mode
  :ensure t
  :mode "\\.jsx\\'"
  :custom ((web-mode-content-types-alist . '(("jsx" . "\\.tsx?\\'")))))

(leaf typescript-mode
  :ensure t
  :mode (("\\.ts\\'" . typescript-mode)
         ("\\.tsx\\'" . web-mode)))

(leaf tide
  :ensure t
  :after (typescript-mode company flycheck)
  :hook ((typescript-mode-hook . tide-setup)
         (typescript-mode-hook . tide-hl-identifier-mode)
         (before-save-hook . tide-format-before-save)))

(leaf lsp-mode
  :url "https://emacs-lsp.github.io/lsp-mode/"
  :ensure t
  :hook (;; replace XXX-mode with concrete major-mode
         ;; e.g.)
         ;; (python-mode . lsp)
         ;; (c++-mode . lsp-deferred)
         (web-mode-hook . lsp-deferred)
         (typescript-mode-hook . lsp-deferred)
         (lsp-mode . lsp-enable-which-key-integration))
  :custom ((lsp-auto-guess-root . t)
           (lsp-prefer-flymake . nil))
  :commands (lsp lsp-deferred)
  :config
  (leaf lsp-ui
    :ensure t
    :commands lsp-ui-mode)

  (leaf lsp-treemacs
    :ensure t
    :commands lsp-treemacs-errors-list)

  (leaf dap-mode
    :ensure t)
  )

;; vi & keybind
(leaf evil
  :ensure t
  :load-path "~/.emacs.d/lisp/"
  :require function
  :hook (after-init-hook . evil-mode)
  :defun (my-forward-char smart-beginning-of-line)
  :custom (evil-undo-system . 'undo-fu)
  :config
  (leaf evil-mc
    :ensure t
    :after evil
    :global-minor-mode global-evil-mc-mode
    :bind ((evil-mc-key-map
            ("M-n" . nil)
            ("M-p" . nil)
            ("C-n" . nil)
            ("C-t" . nil)
            ("C-p" . nil))
           (evil-normal-state-map
            ("SPC m h" . hydra-evil-mc/body)
            ("SPC m R" . evil-mc-undo-all-cursors)))
    :hydra (hydra-evil-mc (:hint nil :pre (evil-mc-pause-cursors))
                          "
^Match^            ^Line-wise^           ^Manual^
^^^^^^----------------------------------------------------
_Z_: match all     _T_: make & go down   _z_: toggle here
_m_: make & next   _N_: make & go up     _r_: remove last
_M_: make & prev   ^ ^                   _R_: remove all
_s_: skip & next   ^ ^                   _p_: pause/resume
_S_: skip & prev

Current Pattern: %`evil-mc-pattern

"
                          ("Z" #'evil-mc-make-all-cursors)
                          ("m" #'evil-mc-make-and-goto-next-match)
                          ("M" #'evil-mc-make-and-goto-prev-match)
                          ("s" #'evil-mc-skip-and-goto-next-match)
                          ("S" #'evil-mc-skip-and-goto-prev-match)
                          ("T" #'evil-mc-make-cursor-move-next-line)
                          ("N" #'evil-mc-make-cursor-move-prev-line)
                          ("z" #'evil-mc-toggle-cursor-here)
                          ("r" #'evil-mc-undo-cursor)
                          ("R" #'evil-mc-undo-all-cursors)
                          ("p" #'evil-mc-toggle-cursors)
                          ("q" #'evil-mc-resume-cursors "quit" :exit t)
                          ("<escape>" #'evil-mc-undo-all-cursors "abort" :exit t)))
  :bind (("C-k" . backward-char)
         ("C-t" . next-line)
         ("C-n" . previous-line)
         ("C-s" . forward-char)
         ("C-]" . xref-find-definitions)
         (evil-normal-state-map
          ("k" . evil-backward-char)
          ("t" . evil-next-line)
          ("n" . evil-previous-line)
          ("s" . evil-forward-char)
          ("C-k" . evil-backward-char)
          ("C-t" . evil-next-line)
          ("C-n" . evil-previous-line)
          ("C-s" . evil-forward-char)
          ("C-i" . smart-beginning-of-line)
          ("C-a" . move-end-of-line)
          ("C-p" . evil-paste-after)
          ("C-S-p" . evil-paste-before)
          ("j" . evil-search-next)
          ("J" . evil-search-previous)
          ("gj" . evil-join)
          ("f" . ace-jump-char-mode)
          ("F" . ace-jump-word-mode)
          ("gl" . ace-jump-line-mode)
          ("za" . hs-toggle-hiding)
          ("zc" . hs-hide-block)
          ("zC" . hs-hide-all)
          ("zo" . hs-show-block)
          ("zO" . hs-show-all)
          )
         (evil-visual-state-map
          ("k" . evil-backward-char)
          ("t" . evil-next-line)
          ("n" . evil-previous-line)
          ("s" . evil-forward-char)
          ("C-i" . smart-beginning-of-line)
          ("C-a" . move-end-of-line)
          )
         (evil-insert-state-map
          ("C-k" . evil-backward-char)
          ("C-t" . evil-next-line)
          ("C-n" . evil-previous-line)
          ("C-s" . my-forward-char)
          ("C-i" . smart-beginning-of-line)
          ("C-a" . move-end-of-line)
          ("C-p" . evil-paste-after)
          ("C-S-p" . evil-paste-before)
          ("C-h" . smart-hungry-delete-backward-char)
          ("M-h" . backward-kill-word)
          ("C-d" . smart-hungry-delete-forward-char)
          ("M-d" . kill-word)
          ))
  )

(leaf which-key
  :ensure t
  :hook (after-init-hook . which-key-mode)
  :custom (which-key-idle-delay . 0)
  :config
  (which-key-setup-minibuffer)
  (which-key-add-key-based-replacements "SPC m" "multiple cursors")
  )

(provide 'init)

;; Local Variables:
;; indent-tabs-mode: nil
;; End:

;;; init.el ends here
