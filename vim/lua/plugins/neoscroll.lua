return {
    'karb94/neoscroll.nvim',
    lazy = true,
    event = 'VeryLazy',
    config = function()
        require('neoscroll').setup()

        local t = {}
        t['<C-b>'] = { 'scroll', { '-vim.api.nvim_win_get_height(0)', 'true', '450' } }
        t['<C-f>'] = { 'scroll', { 'vim.api.nvim_win_get_height(0)', 'true', '450' } }
        t['zt']    = { 'zt', { '250' } }
        t['zz']    = { 'zz', { '250' } }
        t['zb']    = { 'zb', { '250' } }
        require('neoscroll.config').set_mappings(t)
    end
}
