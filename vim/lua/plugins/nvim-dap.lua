return {
    'mfussenegger/nvim-dap',
    dependencies = {
        'rcarriga/nvim-dap-ui',
        {
            'folke/neodev.nvim',
            opts = {
                library = {
                    plugins = { 'nvim-dap-ui' },
                    types = true,
                },
            },
        },
    },
    lazy = true,
    event = 'VeryLazy',
}
