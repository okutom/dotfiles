#!/usr/bin/env bash

ESC=$(printf '\003')

if ! type zsh &>/dev/null; then
  printf "${ESC}[31mSKIP: zsh is not installed.${ESC}[m\n"
  exit
fi

script_dir=$(dirname "${0}")
abs_script_dir=$(cd "${script_dir}" && pwd)

if [ ! -e "${HOME}/.zsh.d" ]; then
  ln -snfv "${abs_script_dir}" "${HOME}/.zsh.d"
fi
ln -snfv "${HOME}/.zsh.d/.zshenv" "${HOME}/.zshenv"
mkdir -p "${HOME}/.zsh.d/.zinit"
git clone https://github.com/zdharma-continuum/zinit.git "${HOME}/.zsh.d/.zinit/bin"

unset abs_script_dir
unset script_dir
